package ch.usi.dslab.bezerra.chirper.util;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.math3.distribution.ZipfDistribution;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class RicardoNetworkGenerator {

   public static class RandomNumberGenerator {
      static RandomNumberGenerator instance;
      public static RandomNumberGenerator getInstance() {
         if (instance == null)
            instance = new RandomNumberGenerator();
         return instance;
      }
      
      Map<Double, double[]> zipfSkewAccTable = new Hashtable<Double, double[]>();

      Random uniformRandom = new Random(0);

      public void changeSeed(long seed) {
         uniformRandom.setSeed(seed);
      }

      private void generateZipfCdfTable(double skew) {
         if (zipfSkewAccTable.containsKey(skew))
            return;

         double[] table = new double[100];
         ZipfDistribution zipf = new ZipfDistribution(100, skew);

         for (int i = 0; i < 99; i++)
            table[i] = zipf.cumulativeProbability(i + 1);
         table[99] = 1.0d;

         zipfSkewAccTable.put(skew, table);
      }

      private double[] getZipfCdfTable(double skew) {
         generateZipfCdfTable(skew);
         return zipfSkewAccTable.get(skew);
      }

      @SuppressWarnings("unused")
      private double getZipfAcc(int x, double skew) {
         generateZipfCdfTable(skew);
         double ret = zipfSkewAccTable.get(skew)[x];
         return ret;
      }

      public int getZipfRandomInt(int max, double skew) {
         return getZipfRandomInt(0, max, skew);
      }

      public int getZipfRandomInt(int min, int max, double skew) {
         int range = max - min;
         double randomNumber = uniformRandom.nextDouble();

         double[] cdfTable = getZipfCdfTable(skew);

         double fraction = 0.0d;
         for (int i = 0; i < 100; i++) {
            if (randomNumber < cdfTable[i]) {
               fraction = ((double) i) / 100.0d;
               break;
            }
         }

         int randomReturn = min + ((int) (fraction * ((double) range)));

         assert (min <= randomReturn && randomReturn <= max);

         return randomReturn;
      }

      public int getUniformRandomInt(int min, int max) {
         return min + uniformRandom.nextInt(1 + max - min);
      }
   }

   public static class RandomUser {
      public int userId;
      public int partitionId;
      public int numFollowers, availableFollowerSlots;
      public int numFolloweds, availableFollowedSlots;
      public List<Integer> followers, followeds;
      public RandomUser() {
         followers = new ArrayList<Integer>();
         followeds = new ArrayList<Integer>();
      }
      public RandomUser(int id) {
         this();
         userId = id;
      }
   }

   public static class RandomPartition {
      int partitionId;
      List<RandomUser> users;

      public RandomPartition(int id) {
         partitionId = id;
         users = new ArrayList<RandomUser>();
      }
   }

   private static double skew = 1.2d;
   private static float ffRatio = 14f / 9f;
   private static int maxFollowers = 20;

   public int correlateFollowedsFromFollowers(int numFollowers) {
      return Math.round(ffRatio * numFollowers);
   }

   public RandomUser getFollowerInSameOrNeighborPartition(RandomUser user, Map<Integer, RandomPartition> allPartitions) {
      RandomNumberGenerator rand = RandomNumberGenerator.getInstance();

      int followerPartitionId = Util.leastOdd(user.partitionId) + rand.getUniformRandomInt(0, 1);
      followerPartitionId = Math.max(1, Math.min(allPartitions.size(), followerPartitionId));
      RandomPartition followerPartition = allPartitions.get(followerPartitionId);

      int round = 0;
      while (round < Integer.MAX_VALUE) {
         for (RandomUser candidateToFollower : followerPartition.users) {
            int extraSlots = candidateToFollower.numFolloweds * round;
            if (candidateToFollower.userId != user.userId 
                  && (candidateToFollower.availableFollowedSlots + extraSlots) > 0
                  && user.followers.contains(candidateToFollower.userId) == false) {
               return candidateToFollower;
            }
         }
         round++;
      }
      return null;
   }

   public List<RandomUser> createNetwork(int numUsers, int numPartitions) {
      RandomNumberGenerator rand = new RandomNumberGenerator();
      Map<Integer, RandomPartition> allPartitions = new HashMap<Integer, RandomPartition>();
      List<RandomUser> allUsers = new ArrayList<RandomUser>();
      for (int i = 0; i < numPartitions; i++) {
         allPartitions.put(i + 1, new RandomPartition(i + 1));
      }
      for (int i = 0; i < numUsers; i++) {
         RandomUser user = new RandomUser(i);
         allUsers.add(user);
         user.numFollowers = user.availableFollowerSlots = rand.getZipfRandomInt(1, maxFollowers, skew);
         user.numFolloweds = user.availableFollowedSlots = correlateFollowedsFromFollowers(user.numFollowers);
         user.partitionId = 1 + user.userId % numPartitions;
         RandomPartition partition = allPartitions.get(user.partitionId);
         partition.users.add(user);
      }
      for (int i = 0; i < numUsers; i++) {
         RandomUser user = allUsers.get(i);
         while (user.availableFollowerSlots > 0) {
            RandomUser follower = getFollowerInSameOrNeighborPartition(user, allPartitions);
            follower.availableFollowedSlots--;
            follower.followeds.add(user.userId);
            user.followers.add(follower.userId);
            user.availableFollowerSlots--;
         }
      }
      return allUsers;
   }

   @SuppressWarnings("unchecked")
   public void saveNetwork(List<RandomUser> allUsers, String filePath) {
      for (RandomUser user : allUsers) {
         System.out.println("User " + user.userId + " (partition " + user.partitionId + "):");
         System.out.println("\t" + user.followers.size() + " followers");
         System.out.println("\t" + user.followeds.size() + " friends");
      }
      JSONObject j_network = new JSONObject();
      j_network.put("numUsers", allUsers.size());
      JSONArray j_usersList = new JSONArray();
      j_network.put("allUsers", j_usersList);
      for (RandomUser user : allUsers) {
         JSONObject j_user = new JSONObject();
         j_usersList.add(j_user);
         j_user.put("userId", user.userId);
         j_user.put("partitionId", user.partitionId);
         JSONArray j_followersList = new JSONArray();
         j_user.put("followers", j_followersList);
         for (int followerId : user.followers)
            j_followersList.add(followerId);
      }

      try {
         FileWriter file = new FileWriter(filePath);
         file.write(j_network.toJSONString());
         file.flush();
         file.close();
      } catch (IOException e) {
         e.printStackTrace();
      }
   }

   public static void main(String[] args) {
      if (args.length != 3) {
         System.out.println("usage: numUsers numPartitions filePath");
         System.exit(1);
      }
      int numUsers      = Integer.parseInt(args[0]);
      int numPartitions = Integer.parseInt(args[1]);
      String filePath   = args[2];
         
      
      RicardoNetworkGenerator rng = new RicardoNetworkGenerator();
      List<RandomUser> network = rng.createNetwork(numUsers, numPartitions);
      rng.saveNetwork(network, filePath);
   }

}
