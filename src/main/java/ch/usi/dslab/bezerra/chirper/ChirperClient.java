/*

 Chirper - A twitter clone based on Eyrie 2 (Optimistic S-SMR)
 Copyright (C) 2014, University of Lugano
 
 This file is part of Chirper.
 
 Chirper is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.chirper;

//import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ch.usi.dslab.bezerra.chirper.util.RicardoNetworkGenerator.RandomUser;
import ch.usi.dslab.bezerra.chirper.util.Util;
import ch.usi.dslab.bezerra.eyrie.Command;
import ch.usi.dslab.bezerra.eyrie.PartitioningOracle;
import ch.usi.dslab.bezerra.eyrie.cs.CallbackHandler;
import ch.usi.dslab.bezerra.eyrie.cs.Client;

import ch.usi.dslab.bezerra.netwrapper.Message;
//import ch.usi.dslab.bezerra.netwrapper.codecs.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
//import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;

//@SuppressWarnings({ "OctalInteger" })
public class ChirperClient {
   
   public static final Logger log = LogManager.getLogger(ChirperClient.class.toString());
   
   Client eyrieClient;
   private Semaphore sendPermits;
   private Random rand = new Random(System.currentTimeMillis());

   public ChirperClient (int clientId, String systemConfigFile, String partitioningFile) {
      this(clientId, systemConfigFile, partitioningFile, ChirperOracle.getInstance(), Integer.MAX_VALUE);
   }
   
   public ChirperClient (int clientId, String systemConfigFile, String partitioningFile, PartitioningOracle oracle, int numPermits) {
      eyrieClient = new Client(clientId, systemConfigFile, partitioningFile, oracle);
      sendPermits = new Semaphore(numPermits);
   }
   
   public int getId() {
      return eyrieClient.getId();
   }
   
   void getSendPermit() {
      try {
         sendPermits.acquire();
      } catch (InterruptedException e) {
         e.printStackTrace();
         System.exit(1);
      }
   }
   
   void addSendPermit() {
      sendPermits.release();
   }
   
   public static class FollowersCache {
      private static Map<Integer, FollowersCache> allCaches = new ConcurrentHashMap<Integer, FollowersCache>();
      synchronized public static FollowersCache getCacheForUser(int userId) {
         FollowersCache cache = allCaches.get(userId);
         if (cache == null) {
            cache = new FollowersCache();
            allCaches.put(userId, cache);
         }
         return cache;
      }
      private Set<Integer> followers = new HashSet<Integer>();
      synchronized public void setCache(List<Integer> followersList) {
         this.followers = new HashSet<Integer>(followersList);
      }
      synchronized public void addFollowers(List<Integer> followersToAdd) {
         followers.addAll(followersToAdd);
      }
      synchronized public void removeFollowers(List<Integer> followersToRemove) {
         followers.removeAll(followersToRemove);
      }
      synchronized public void updateCache(List<Integer> followersToAdd, List<Integer> followersToRemove) {
         addFollowers(followersToAdd);
         removeFollowers(followersToRemove);
      }
      synchronized public List<Integer> getFollowersCache() {
         return new ArrayList<Integer>(followers);
      }
   }

   public static class PostReplyHandler implements CallbackHandler {
      CallbackHandler userHandler;
      Object          userContext;
      ChirperClient   client;
      int             userId;
      Post            post;
      boolean         optimistic;
      // the command's followerCache never decreases, ensuring termination of the request execution
      FollowersCache  commandFollowerCache;
      FollowersCache  clientFollowerCache;
      
      public PostReplyHandler(CallbackHandler userHandler, Object userContext, ChirperClient client, int userId,
            Post post, boolean optimistic) {
         this.userHandler = userHandler;
         this.userContext = userContext;
         this.client      = client;
         this.userId      = userId;
         this.post        = post;
         this.optimistic  = optimistic;
         clientFollowerCache  = FollowersCache.getCacheForUser(userId);
         commandFollowerCache = new FollowersCache();
         commandFollowerCache.setCache(clientFollowerCache.getFollowersCache());
      }

      @SuppressWarnings("unchecked")
      public void handleCallback(Object returnedData, Object context) {
         Message reply = (Message) returnedData;
         String status = (String) reply.getNext();
        
         // System.out.println("Received " + (optimistic ? "opt" : "cons") + " reply for POST with status \"" + status + "\"");
 
         Message replyIfOK = new Message("OK");
         replyIfOK.copyTimelineStamps(reply);
         
         if (optimistic == true && status.equals("OK")) {
            // System.out.println("handling POST reply with status \"" + status + "\"");
            userHandler.handleCallback(replyIfOK, userContext);
         }
         
         if (optimistic == false) {
            List<Integer> followersToAdd = (List<Integer>) reply.getNext();
            List<Integer> followersToRemove = (List<Integer>) reply.getNext();
            clientFollowerCache.updateCache(followersToAdd, followersToRemove);
            
            if (status.equals("RETRY")) {
               // System.out.println("Retrying post with more followers");
               commandFollowerCache.addFollowers(followersToAdd);
               List<Integer> estimatedFollowers = commandFollowerCache.getFollowersCache();
               Command command = new Command(CommandType.POST, userId, estimatedFollowers, post);
               client.eyrieClient.sendCommandAsync(this, userContext, command);
            }
            else { // if status.equal("OK")
               // System.out.println("handling POST reply with status \"" + status + "\"");
               userHandler.handleCallback(replyIfOK, userContext);
            }

         }

      }
   }
   
   public void loadSocialNetworkIntoCache(String file) {
      List<RandomUser> randomUsers = Util.loadSocialNetwork(file);
      for (RandomUser user : randomUsers) {
         FollowersCache followersCache = FollowersCache.getCacheForUser(user.userId);
         followersCache.addFollowers(user.followers);
      }
   }
   
   void sendTTYCommand(String cmdtype, int arg1, int uid2, String data) {
      if (cmdtype.equalsIgnoreCase("burstpost") == false && cmdtype.equalsIgnoreCase("bursttl") == false) {
         System.out.print("Waiting for permit...");
         getSendPermit();
         System.out.println(" got it.");
      }
      
      System.out.println(String.format("Sending command %s %d %d %s", cmdtype, arg1, uid2, data));

      if (cmdtype.equalsIgnoreCase("new")) {
         CallbackHandler newcbhCons = new CallbackHandler() {
            public void handleCallback(Object returnedData, Object context) {
               addSendPermit();
               int  uid = (Integer) context;
               Message reply     = (Message) returnedData;
               String  retval    = (String ) reply.getNext();
               System.out.println("(CONS) Successfully created User " + uid + " : " + retval);
            }
         };
         CallbackHandler newcbhOpt = new CallbackHandler() {
            public void handleCallback(Object returnedData, Object context) {
               int  uid = (Integer) context;
               Message reply     = (Message) returnedData;
               String  retval    = (String ) reply.getNext();
               System.out.println("(OPT) Successfully created User " + uid + " : " + retval);
            }
         };
         createUserNB(arg1, newcbhOpt, newcbhCons, arg1);
      }

      else if (cmdtype.equalsIgnoreCase("f")) {
         CallbackHandler followcbhCons = new CallbackHandler() {            
            public void handleCallback(Object returnedData, Object context) {
               addSendPermit();
               String  reqstring = (String)  context;
               Message reply     = (Message) returnedData;
               String  retval    = (String ) reply.getNext();
               System.out.println("(CONS) Successfully done: " + reqstring + " : " + retval);
            }
         };
         CallbackHandler followcbhOpt = new CallbackHandler() {            
            public void handleCallback(Object returnedData, Object context) {
               String  reqstring = (String)  context;
               Message reply     = (Message) returnedData;
               String  retval    = (String ) reply.getNext();
               System.out.println("(OPT) Successfully done: " + reqstring + " : " + retval);
            }
         };
         followNB(arg1, uid2, followcbhOpt, followcbhCons, "User " + arg1 + " follow user " + uid2);
      }

      else if (cmdtype.equalsIgnoreCase("uf")) {
         CallbackHandler unfollowcbhCons = new CallbackHandler() {            
            public void handleCallback(Object returnedData, Object context) {
               addSendPermit();
               String  reqstring = (String)  context;
               Message reply     = (Message) returnedData;
               String  retval    = (String ) reply.getNext();
               System.out.println("(CONS) Successfully done: " + reqstring + " : " + retval);
            }
         };
         CallbackHandler unfollowcbhOpt = new CallbackHandler() {            
            public void handleCallback(Object returnedData, Object context) {
               String  reqstring = (String)  context;
               Message reply     = (Message) returnedData;
               String  retval    = (String ) reply.getNext();
               System.out.println("(OPT) Successfully done: " + reqstring + " : " + retval);
            }
         };
         unfollowNB(arg1, uid2, unfollowcbhOpt, unfollowcbhCons, "User " + arg1 + " unfollow user " + uid2);
      }

      else if (cmdtype.equalsIgnoreCase("p")) {
         CallbackHandler postcbhCons = new CallbackHandler() {            
            public void handleCallback(Object returnedData, Object context) {
               addSendPermit();
               String  reqstring = (String)  context;
               Message reply     = (Message) returnedData;
               String  retval    = (String ) reply.getNext();
               System.out.println("(CONS) Successfully done: " + reqstring + " : " + retval);
            }
         };
         CallbackHandler postcbhOpt = new CallbackHandler() {            
            public void handleCallback(Object returnedData, Object context) {
               String  reqstring = (String)  context;
               Message reply     = (Message) returnedData;
               String  retval    = (String ) reply.getNext();
               System.out.println("(OPT) Successfully done: " + reqstring + " : " + retval);
            }
         };
         postNB(arg1, new Post(eyrieClient.getId(), arg1, System.currentTimeMillis(), data.getBytes()), postcbhOpt, postcbhCons, "User " + arg1 + " posted " + data);
      }

      else if (cmdtype.equalsIgnoreCase("tl")) {
         CallbackHandler timelinecbhCons = new CallbackHandler() {            
            @SuppressWarnings("unchecked")
            public void handleCallback(Object returnedData, Object context) {
               addSendPermit();
               String     reqstring = (String    )  context;
               Message    reply = (Message) returnedData;
               List<Post> timeline  = (List<Post>) reply.getNext();
               System.out.println("(CONS) Successfully done: " + reqstring);
               System.out.println(timeline.size() + " posts in the timeline");
               for (Post post: timeline)
                  System.out.println(post);
            }
         };
         CallbackHandler timelinecbhOpt = new CallbackHandler() {            
            @SuppressWarnings("unchecked")
            public void handleCallback(Object returnedData, Object context) {
               String     reqstring = (String    )  context;
               Message    reply = (Message) returnedData;
               List<Post> timeline  = (List<Post>) reply.getNext();
               System.out.println("(OPT) Successfully done: " + reqstring);
               System.out.println(timeline.size() + " posts in the timeline");
               for (Post post: timeline)
                  System.out.println(post);
            }
         };
         getTimelineNB(arg1, timelinecbhOpt, timelinecbhCons, "user " + arg1 + " getTimeline");
      }
      
      else if (cmdtype.equalsIgnoreCase("pl")) {
         CallbackHandler postslistscbhCons = new CallbackHandler() {            
            @SuppressWarnings("unchecked")
            public void handleCallback(Object returnedData, Object context) {
               addSendPermit();
               String     reqstring = (String)  context;
               Message    reply = (Message) returnedData;
               List<Post> posts  = (List<Post>) reply.getNext();
               System.out.println("(CONS) Successfully done: " + reqstring);
               System.out.println(posts.size() + " posts of user");
               for (Post post: posts)
                  System.out.println(post);
            }
         };
         CallbackHandler postslistscbhOpt = new CallbackHandler() {            
            @SuppressWarnings("unchecked")
            public void handleCallback(Object returnedData, Object context) {
               String     reqstring = (String)  context;
               Message    reply = (Message) returnedData;
               List<Post> posts  = (List<Post>) reply.getNext();
               System.out.println("(OPT) Successfully done: " + reqstring);
               System.out.println(posts.size() + " posts of user");
               for (Post post: posts)
                  System.out.println(post);
            }
         };
         getPostsNB(arg1, postslistscbhOpt, postslistscbhCons, "user " + arg1 + " getTimeline");
      }
      
      else if (cmdtype.equalsIgnoreCase("fr")) {
         CallbackHandler followerscbhCons = new CallbackHandler() {            
            @SuppressWarnings("unchecked")
            public void handleCallback(Object returnedData, Object context) {
               addSendPermit();
               String     reqstring = (String)  context;
               Message    reply = (Message) returnedData;
               List<Integer> followers  = (List<Integer>) reply.getNext();
               System.out.println("(CONS) Successfully done: " + reqstring);
               System.out.println(followers.size() + " followers");
               for (int follower: followers)
                  System.out.println("\t" + follower);
            }
         };
         CallbackHandler followerscbhOpt = new CallbackHandler() {            
            @SuppressWarnings("unchecked")
            public void handleCallback(Object returnedData, Object context) {
               String     reqstring = (String)  context;
               Message    reply = (Message) returnedData;
               List<Integer> followers  = (List<Integer>) reply.getNext();
               System.out.println("(OPT) Successfully done: " + reqstring);
               System.out.println(followers.size() + " followers");
               for (int follower: followers)
                  System.out.println("\t" + follower);
            }
         };
         getFollowersNB(arg1, followerscbhOpt, followerscbhCons, "user " + arg1 + " getFollowers");
      }
      
      else if (cmdtype.equalsIgnoreCase("fd")) {
         CallbackHandler followedschbCons = new CallbackHandler() {
            @SuppressWarnings("unchecked")
            public void handleCallback(Object returnedData, Object context) {
               addSendPermit();
               String     reqstring = (String)  context;
               Message    reply = (Message) returnedData;
               List<Integer> followeds  = (List<Integer>) reply.getNext();
               System.out.println("(CONS) Successfully done: " + reqstring);
               System.out.println(followeds.size() + " followeds");
               for (int followed: followeds)
                  System.out.println("\t" + followed);
            }
         };
         CallbackHandler followedschbOpt = new CallbackHandler() {            
            @SuppressWarnings("unchecked")
            public void handleCallback(Object returnedData, Object context) {
               String     reqstring = (String)  context;
               Message    reply = (Message) returnedData;
               List<Integer> followeds  = (List<Integer>) reply.getNext();
               System.out.println("(OPT) Successfully done: " + reqstring);
               System.out.println(followeds.size() + " followeds");
               for (int followed: followeds)
                  System.out.println("\t" + followed);
            }
         };
         getFollowedsNB(arg1, followedschbOpt, followedschbCons, "user " + arg1 + " getFolloweds");
      }
      
      else if (cmdtype.equalsIgnoreCase("burstpost")) {
         for (int i = 0 ; i < arg1 ; i++) {
            int puid1 = rand.nextInt(1000);
            String postdata = "qwertyuiopasdfghjkl;zxcvbnm";
            sendTTYCommand("p", puid1, 0, postdata);
         }
      }
      
      else if (cmdtype.equalsIgnoreCase("bursttl")) {
         for (int i = 0 ; i < arg1 ; i++) {
            int tluid1 = rand.nextInt(1000);
            sendTTYCommand("tl", tluid1, 0, null);
         }
      }
      
      else {
         System.err.println("wrong command type");
         return;
      }
   }
   
   
   
   // ================================
   // ChirperClient User API
   // ================================
   
   public String createUser(int userId) {
      log.info("Calling createUser for user id {}.", userId);
      Command command = new Command(CommandType.NEWUSER, userId);
      Message reply = eyrieClient.sendCommandSync(command);
      return (String) reply.getNext();
   }
   
   public void createUserNB(int userId, CallbackHandler optimisticHandler, CallbackHandler conservativeHandler, Object context) {
      log.info("Calling createUserNB for user id {}.", userId);
      Command command = new Command(CommandType.NEWUSER, userId);
      eyrieClient.sendCommandAsync(optimisticHandler, conservativeHandler, context, command);
   }

   @SuppressWarnings("unchecked")
   public String post(int userId, Post post) {
      log.info("User {} requesting to post {}.", userId, post);
      FollowersCache clientFollowerCache  = FollowersCache.getCacheForUser(userId);
      FollowersCache commandFollowerCache = new FollowersCache();
      commandFollowerCache.setCache(clientFollowerCache.getFollowersCache());
      String status = "none";
      while (status.equals("OK") == false) {
         List<Integer> estimatedFollowers = commandFollowerCache.getFollowersCache();
         Command command = new Command(CommandType.POST, userId, estimatedFollowers, post);
         Message reply = eyrieClient.sendCommandSync(command);
         status = (String) reply.getNext();
         List<Integer> followersToAdd = (List<Integer>) reply.getNext();
         List<Integer> followersToRemove = (List<Integer>) reply.getNext();
         clientFollowerCache.updateCache(followersToAdd, followersToRemove);
         if (status.equals("RETRY")) {
            commandFollowerCache.addFollowers(followersToAdd);
         }
      }
      return "OK";
   }
   
   public void postNB(int userId, Post post, CallbackHandler optimisticHandler, CallbackHandler conservativeHandler, Object context) {
      log.info("User {} posting {}.", userId, post);
      List<Integer> estimatedFollowers = FollowersCache.getCacheForUser(userId).getFollowersCache();
      Command command = new Command(CommandType.POST, userId, estimatedFollowers, post);
      CallbackHandler postReplyHandlerOpt  = new PostReplyHandler(optimisticHandler  , context, this, userId, post, true);
      CallbackHandler postReplyHandlerCons = new PostReplyHandler(conservativeHandler, context, this, userId, post, false);
      eyrieClient.sendCommandAsync(postReplyHandlerOpt, postReplyHandlerCons, context, command);
   }

   public String follow(int followerId, int followedId) {
      log.info("User {} requesting to follow {}.", followerId, followedId);
      Command command = new Command(CommandType.FOLLOW, followerId, followedId);
      Message reply = eyrieClient.sendCommandSync(command);
      return (String) reply.getNext();
   }
   
   public void followNB(int followerId, int followedId, CallbackHandler optimisticHandler, CallbackHandler conservativeHandler, Object context) {
      log.info("User {} followNB'ing user {}.", followerId, followedId);
      Command command = new Command(CommandType.FOLLOW, followerId, followedId);
      eyrieClient.sendCommandAsync(optimisticHandler, conservativeHandler, context, command);
   }
   
   public void follow_NOP_NB(int followerId, int followedId, CallbackHandler optimisticHandler, CallbackHandler conservativeHandler, Object context) {
      log.info("User {} follow_nop_NB'ing user {}.", followerId, followedId);
      Command command = new Command(CommandType.FOLLOW_NOP, followerId, followedId);
      eyrieClient.sendCommandAsync(optimisticHandler, conservativeHandler, context, command);
   }
   
   public String unfollow(int followerId, int followedId) {
      log.info("User {} requesting to unfollow {}.", followerId, followedId);
      Command command = new Command(CommandType.UNFOLLOW, followerId, followedId);
      Message reply = eyrieClient.sendCommandSync(command);
      return (String) reply.getNext();
   }
   
   public void unfollowNB(int followerId, int followedId, CallbackHandler optimisticHandler, CallbackHandler conservativeHandler, Object context) {
      log.info("User {} unfollowNB'ing user {}.", followerId, followedId);
      Command command = new Command(CommandType.UNFOLLOW, followerId, followedId);
      eyrieClient.sendCommandAsync(optimisticHandler, conservativeHandler, context, command);
   }
   
   public void unfollow_NOP_NB(int followerId, int followedId, CallbackHandler optimisticHandler, CallbackHandler conservativeHandler, Object context) {
      log.info("User {} unfollowNB'ing user {}.", followerId, followedId);
      Command command = new Command(CommandType.UNFOLLOW_NOP, followerId, followedId);
      eyrieClient.sendCommandAsync(optimisticHandler, conservativeHandler, context, command);
   }
   
   @SuppressWarnings("unchecked")
   public List<Post> getTimeline(int userId) {
      log.info("User {} requesting their timeline.", userId);
      Command command = new Command(CommandType.GETTIMELINE, userId);
      Message reply = eyrieClient.sendCommandSync(command);
      List<Post> timeline = (List<Post>) reply.getNext();
      // needs to be sorted (sorting is done in the client to save server's cpu):
      Collections.sort(timeline, Post.comparatorNewestFirst);
      
      return timeline;
   }
   
   public void getTimelineNB(int userId, CallbackHandler optimisticHandler, CallbackHandler conservativeHandler, Object context) {
      log.info("User {} requesting their.", userId);
      Command command = new Command(CommandType.GETTIMELINE, userId);
      eyrieClient.sendCommandAsync(optimisticHandler, conservativeHandler, context, command);
   }
   
   public void getPostsNB(int userId, CallbackHandler optimisticHandler, CallbackHandler conservativeHandler, Object context) {
      log.info("User {} requesting their posts.", userId);
      Command command = new Command(CommandType.GETPOSTS, userId);
      eyrieClient.sendCommandAsync(optimisticHandler, conservativeHandler, context, command);
   }
   
   public void getFollowersNB(int userId, CallbackHandler optimisticHandler, CallbackHandler conservativeHandler, Object context) {
      log.info("User {} requesting their followers.", userId);
      Command command = new Command(CommandType.GETFOLLOWERLIST, userId);
      eyrieClient.sendCommandAsync(optimisticHandler, conservativeHandler, context, command);
   }
   
   public void getFollowedsNB(int userId, CallbackHandler optimisticHandler, CallbackHandler conservativeHandler, Object context) {
      log.info("User {} requesting their followeds.", userId);
      Command command = new Command(CommandType.GETFOLLOWEDLIST, userId);
      eyrieClient.sendCommandAsync(optimisticHandler, conservativeHandler, context, command);
   }
//
//   public int init() {
//      return 0;
//   }
//
//   public int destroy() {
//      return 0;
//   }
//   
//   // parses server list in the format addr:port:part[,addr2:port2:part2[,...]]
//   // e.g.: node1:60001:1,node2:60002:1,node3:60003:2,node4:60004:2
//   public static Map<Integer, PartitionServers> parseServerList(String serverList) {
//      String[] servers = serverList.split(",");
//      Map<Integer, PartitionServers> partServers = new HashMap<Integer, PartitionServers>(servers.length);
//      for (String server : servers) {
//         String addr         = server.split(":")[0];
//         String portStr      = server.split(":")[1];
//         String partitionStr = server.split(":")[2];
//         int port      = Integer.parseInt(portStr);
//         int partition = Integer.parseInt(partitionStr);
//         
//         PartitionServers ps = partServers.get(partition);
//         if (ps == null) {
//            ps = new PartitionServers();
//            partServers.put(partition, ps);
//         }
//         
//         ps.add(new Address(addr, port));
//      }
//      return partServers;
//   }

   public void runInteractive(ChirperClient chirperClient){
      Scanner scan = new Scanner(System.in);
      String  input;

      System.out.println("Type: new/p/f/uf/tl/fr/fd/pl/burstpost/bursttl [uid1] [uid2] [data_to_post] [burst_length]:");
      input = scan.nextLine();
      while (input.equalsIgnoreCase("end") == false) {
         try {
            String[] parts = input.split(" ");
            String cmdtype = parts[0];
            int    arg1    = 0;
            int    uid2    = 0;
            String data    = null;

            boolean validCommand = true;

            if (cmdtype.equalsIgnoreCase("new")) {
               arg1 = Integer.parseInt(parts[1]);                  
            }
            else if (cmdtype.equalsIgnoreCase("p")) {
               arg1 = Integer.parseInt(parts[1]);
               data = parts[2];
            }
            else if (cmdtype.equalsIgnoreCase("f")) {
               arg1 = Integer.parseInt(parts[1]);
               uid2 = Integer.parseInt(parts[2]);
            }
            else if (cmdtype.equalsIgnoreCase("uf")) {
               arg1 = Integer.parseInt(parts[1]);
               uid2 = Integer.parseInt(parts[2]);
            }
            else if (cmdtype.equalsIgnoreCase("tl")) {
               arg1 = Integer.parseInt(parts[1]);
            }
            else if (cmdtype.equalsIgnoreCase("fr")) {
               arg1 = Integer.parseInt(parts[1]);
            }
            else if (cmdtype.equalsIgnoreCase("fd")) {
               arg1 = Integer.parseInt(parts[1]);
            }
            else if (cmdtype.equalsIgnoreCase("pl")) {
               arg1 = Integer.parseInt(parts[1]);
            }
            else if (cmdtype.equalsIgnoreCase("burstpost")) {
               arg1 = Integer.parseInt(parts[1]);
            }
            else if (cmdtype.equalsIgnoreCase("bursttl")) {
               arg1 = Integer.parseInt(parts[1]);
            }
            else {
               System.out.println("Invalid command.");
               validCommand = false;
            }
            
            if (validCommand)
               chirperClient.sendTTYCommand(cmdtype, arg1, uid2, data);
            
            System.out.println("Type: new/p/f/uf/tl/fr/fd/pl/burstpost/bursttl [uid1] [uid2] [data_to_post] [burst_length]:");
         } catch (Exception e) {
            System.err.println("Probably invalid input.");
            e.printStackTrace();
            System.out.println("Type: new/p/f/uf/tl/fr/fd/pl/burstpost/bursttl [uid1] [uid2] [data_to_post] [burst_length]:");
         }
         
         input = scan.nextLine();
         
      }
      scan.close();
   }
   
   public static void main(String[] args) {
/*
      
      DescriptiveStatistics lz4Times = new DescriptiveStatistics();
      DescriptiveStatistics lz4Sizes = new DescriptiveStatistics();
      DescriptiveStatistics plainTimes = new DescriptiveStatistics();
      DescriptiveStatistics plainSizes = new DescriptiveStatistics();
      

      Codec plainCodec = new CodecUncompressedKryo();
      Codec lz4Codec = new CodecLZ4();

      for (int i = 0 ; i < 10000 ; i++) {
         plainCodec.getByteBufferWithLengthHeader(new Message("OLAAAA!!!"));
         lz4Codec.getByteBufferWithLengthHeader(new Message("OEEEE!!!"));
      }
      java.nio.ByteBuffer lz4BB = null;
      java.nio.ByteBuffer plainBB = null;
      User user = null;
      for (int i = 0 ; i < 500 ; i++) {
         if (i % 100 == 0) System.out.println(i);
         user = new User(i);
         for (int j = 0 ; j < 0 ; j++) {
            user.follow(j);
            user.addFollower(j*19);
         }
         for (int j = 0 ; j < 0 ; j++) {
            Post post = new Post(j, System.currentTimeMillis(), new byte[140]);
            user.post(post);
            user.addToMaterializedTimeline(post);
         }
         Message userMessage = new Message(user);
         
         long startLZ4 = System.nanoTime();
         lz4BB = lz4Codec.getByteBufferWithLengthHeader(userMessage);
         long endLZ4 = System.nanoTime();
         double latencyLZ4 = endLZ4 - startLZ4;
         lz4Times.addValue(latencyLZ4);
         lz4Sizes.addValue(lz4BB.limit());
         
         long startPlain = System.nanoTime();
         plainBB = plainCodec.getByteBufferWithLengthHeader(userMessage);
         long endPlain = System.nanoTime();
         double latencyPlain = endPlain - startPlain;
         plainTimes.addValue(latencyPlain);
         plainSizes.addValue(plainBB.limit());
      }

      System.out.print("plain:");
      System.out.println(String.format("\tsize: %.2f - %.2f - %.2f - %.2f - %.2f",
            plainSizes.getPercentile(1),
            plainSizes.getPercentile(25),
            plainSizes.getPercentile(50),
            plainSizes.getPercentile(75),
            plainSizes.getPercentile(99)));
      System.out.println(String.format("\ttime: %.2f - %.2f - %.2f - %.2f - %.2f",
            plainTimes.getPercentile(1)/1e3,
            plainTimes.getPercentile(25)/1e3,
            plainTimes.getPercentile(50)/1e3,
            plainTimes.getPercentile(75)/1e3,
            plainTimes.getPercentile(99)/1e3));
      System.out.print("lz4:");
      System.out.println(String.format("\tsize: %.2f - %.2f - %.2f - %.2f - %.2f",
            lz4Sizes.getPercentile(1),
            lz4Sizes.getPercentile(25),
            lz4Sizes.getPercentile(50),
            lz4Sizes.getPercentile(75),
            lz4Sizes.getPercentile(99)));
      System.out.println(String.format("\ttime: %.2f - %.2f - %.2f - %.2f - %.2f",
            lz4Times.getPercentile(1)/1e3,
            lz4Times.getPercentile(25)/1e3,
            lz4Times.getPercentile(50)/1e3,
            lz4Times.getPercentile(75)/1e3,
            lz4Times.getPercentile(99)/1e3));
      
      if (true) return;
      
*/
      
      log.info("Entering ChirperClient.main(...)");
      
      int     clientId             = Integer.parseInt(args[0]);
      String  systemConfigFile     = args[1];
      String  partitionsConfigFile = args[2];
      int     numPermits           = args.length > 3 ? Integer.parseInt(args[3]) : 1;
      
      ChirperOracle oracle = ChirperOracle.getInstance();
      ChirperClient chirperClient = new ChirperClient(clientId, systemConfigFile, partitionsConfigFile, oracle, numPermits);

      // System.out.println("Running command: chirperClient.sendTTYCommand(\"NEW\", 0, 0, null);");
      // chirperClient.sendTTYCommand("NEW", Integer.MAX_VALUE, 0, null);
      chirperClient.runInteractive(chirperClient);

   }
}
