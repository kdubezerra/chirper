/*

 Chirper - A twitter clone based on Eyrie 2 (Optimistic S-SMR)
 Copyright (C) 2014, University of Lugano
 
 This file is part of Chirper.
 
 Chirper is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.chirper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ch.usi.dslab.bezerra.eyrie.Command;
import ch.usi.dslab.bezerra.eyrie.ObjId;
import ch.usi.dslab.bezerra.eyrie.PRObject;
import ch.usi.dslab.bezerra.eyrie.Partition;
import ch.usi.dslab.bezerra.eyrie.PartitioningOracle;

public class ChirperOracle extends PartitioningOracle {
   
   // ============================
   // ChirperOracle is a singleton
   private static ChirperOracle instance;
   private ChirperOracle(){}
   public static ChirperOracle getInstance() {
      if (instance == null)
         instance = new ChirperOracle();
      return instance;
   }
   // ============================
   
//   Map<Integer, List<Integer>> followedUsersCacheTable = new Hashtable<Integer, List<Integer>>();
//   
//   public List<Integer> getUserFollowedListCache(int userId) {
//      return followedUsersCacheTable.get(userId);
//   }
//   
//   public void setUserFollowedListCache(int userId, List<Integer> followedUsersList) {
//      followedUsersCacheTable.put(userId, followedUsersList);
//   }
   
   public Partition mapUserIdToPartition(int uid) {
      List<Partition> allPartitions = Partition.getPartitionsList();
      Partition userPartition = allPartitions.get(uid % allPartitions.size());
      return userPartition;
   }
   
   public Partition mapUserToPartition(User user) {
      return mapUserIdToPartition(user.id);
   }

   public List<Partition> mapUserIdToDestinations(int userId) {
      return mapUserIdsToDestinations(userId);
   }
   
   public List<Partition> mapUserIdsToDestinations(int... userIds) {
      List<Partition> dests = new ArrayList<Partition>(userIds.length);
      for (int userId : userIds)
         dests.add(mapUserIdToPartition(userId));
      return dests;
   }

   public List<Partition> mapUserIdListToPartitionList(List<Integer> userIds) {
      List<Partition> partitions = new ArrayList<Partition>();
      for (int userId : userIds) {
         Partition userPartition = mapUserIdToPartition(userId);
         if (partitions.contains(userPartition) == false)
            partitions.add(mapUserIdToPartition(userId));
      }
         
      return partitions;
   }

   @Override
   public Partition getObjectPlacement(Command cmd) {
      return null;
   }
   
   @Override
   public Partition getObjectPlacement(PRObject obj) {
      User user = (User) obj;
      return mapUserIdToPartition(user.id);
   }

   @SuppressWarnings("unchecked")
   @Override
   public Prophecy getProphecy(Command cmd) {
      // | RequestType.GETTIMELINE | userId |
      
      Prophecy prophecy = null;
      
      int reqType = (Integer) cmd.getItem(0);
      int userId  = (Integer) cmd.getItem(1);
      
      switch (reqType) {
         case CommandType.NEWUSER : {
            prophecy = new Prophecy(Partition.getPartitionsList(), null);            
            break;
         }

         case CommandType.POST : {
            // new Command(RequestType.POST_MATERIALIZE, userId, sentFollowerList);
            
            List<Integer> involvedUsers = new ArrayList<Integer>((List<Integer>) cmd.getItem(2));
            involvedUsers.add(userId);
            prophecy = new Prophecy(mapUserIdListToPartitionList(involvedUsers), null);
            break;
         }

         case CommandType.FOLLOW :
         case CommandType.UNFOLLOW :
         case CommandType.FOLLOW_NOP :
         case CommandType.UNFOLLOW_NOP : {
            int followerId = userId;
            int followedId = (Integer) cmd.getItem(2);
            prophecy = new Prophecy(mapUserIdsToDestinations(followerId, followedId), null);
            break;
         }

         case CommandType.GETPOSTS :
         case CommandType.GETFOLLOWERLIST :
         case CommandType.GETFOLLOWEDLIST :
         case CommandType.GETTIMELINE : {
            prophecy = new Prophecy(mapUserIdToDestinations(userId), null);
            break;
         }
         
         default : {
            break;
         }
         
      }
      
      return prophecy;
   }

   @Override
   public Prophecy getProphecyForNonBlocking(Command cmd) {
      return null;
   }

   @Override
   @SuppressWarnings("unchecked")
   public Set<ObjId> getEarlyObjectsIds(Command cmd) {
      // | RequestType.GETTIMELINE | userId |
      
      Set<ObjId> earlyObjects = new HashSet<ObjId>();
      
      int reqType = (Integer) cmd.getItem(0);
      int userId  = (Integer) cmd.getItem(1);
      
      switch (reqType) {
         case CommandType.NEWUSER : {
            break;
         }

         case CommandType.POST : {
            // new Command(RequestType.POST_MATERIALIZE, userId, sentFollowerList);
            
            earlyObjects.add(User.getUser(userId).getId());            
            List<Integer> followers = (List<Integer>) cmd.getItem(2);
            for (int followerId : followers)
               earlyObjects.add(User.getUser(followerId).getId());            
            break;
         }

         case CommandType.FOLLOW :
         case CommandType.UNFOLLOW :
         case CommandType.FOLLOW_NOP :
         case CommandType.UNFOLLOW_NOP : {
            int followerId = userId;
            int followedId = (Integer) cmd.getItem(2);
            earlyObjects.add(User.getUser(followerId).getId());
            earlyObjects.add(User.getUser(followedId).getId());
            break;
         }

         case CommandType.GETPOSTS :
         case CommandType.GETFOLLOWERLIST :
         case CommandType.GETFOLLOWEDLIST :
         case CommandType.GETTIMELINE : {
            earlyObjects.add(User.getUser(userId).getId());
            break;
         }
         
         default : {
            break;
         }
         
      }
      
      return earlyObjects;
   }
   
//   @Override
   public boolean knowsAllEarlyObjectIds(Command cmd) {
      // | RequestType.GETTIMELINE | userId |
      
      boolean returnValue;
      
      int reqType = (Integer) cmd.getItem(0);
      
      switch (reqType) {
         case CommandType.NEWUSER : {
            returnValue = false;
            break;
         }

         case CommandType.POST :
         case CommandType.FOLLOW :
         case CommandType.UNFOLLOW :
         case CommandType.FOLLOW_NOP :
         case CommandType.UNFOLLOW_NOP :
         case CommandType.GETPOSTS :
         case CommandType.GETFOLLOWERLIST :
         case CommandType.GETFOLLOWEDLIST :
         case CommandType.GETTIMELINE : {
            returnValue = true;
            break;
         }

         default : {
            returnValue = false;
            break;
         }
         
      }
      
      return returnValue;
   }

}
