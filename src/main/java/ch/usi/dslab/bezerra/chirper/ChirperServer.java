/*

 Chirper - A twitter clone based on Eyrie 2 (Optimistic S-SMR)
 Copyright (C) 2014, University of Lugano
 
 This file is part of Chirper.
 
 Chirper is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.chirper;

import ch.usi.dslab.bezerra.chirper.util.RicardoNetworkGenerator.RandomUser;
import ch.usi.dslab.bezerra.chirper.util.Util;
import ch.usi.dslab.bezerra.eyrie.Command;
import ch.usi.dslab.bezerra.eyrie.Partition;
import ch.usi.dslab.bezerra.eyrie.PartitioningOracle;
import ch.usi.dslab.bezerra.eyrie.StateMachine;
import ch.usi.dslab.bezerra.netwrapper.Message;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;

public class ChirperServer extends StateMachine {
   public static final Logger log = LogManager.getLogger(ChirperServer.class.toString());
   
   int creationSeq = 0;

   boolean running = true;
   
   Thread serverThread;

   public void stop() {
      running = false;
   }
   
   @Override
   public Message executeCommand(Command cmd) {
      return handleCommand(cmd);
   }
   
   @SuppressWarnings("unchecked")
   Message handleCommand(Command command) {
      
      // format: | reqType | userId | ...
      
      int cmdType = (Integer) command.getNext();
      int userId  = (Integer) command.getNext();
      
      Message reply = null;
      switch (cmdType) {
         case CommandType.NEWUSER : {
            reply = createUser(userId);
            break;
         }

         case CommandType.POST : {
            // new Command(CommandType.POST_MATERIALIZE, userId, postFollowersList, post);
            List<Integer> followers = (List<Integer>) command.getNext();
            Post post = (Post) command.getNext();
            reply = post(userId, followers, post);
            break;
         }
            
         case CommandType.FOLLOW : {
            int followerId = userId;
            int followedId = (Integer) command.getNext();
            
            reply = follow(followerId, followedId);
            break;
         }
         
         case CommandType.FOLLOW_NOP : {
            int followerId = userId;
            int followedId = (Integer) command.getNext();
            
            reply = follow_nop(followerId, followedId);
            break;
         }
            
         case CommandType.UNFOLLOW : {
            int followerId = userId;
            int followedId = (Integer) command.getNext();
            
            reply = unfollow(followerId, followedId);
            break;
         }
         
         case CommandType.UNFOLLOW_NOP : {
            int followerId = userId;
            int followedId = (Integer) command.getNext();
            
            reply = unfollow_nop(followerId, followedId);
            break;
         }
            
         case CommandType.GETTIMELINE : {
            reply = getTimeline(userId);
            break;
         }
         
         case CommandType.GETFOLLOWEDLIST : {
            reply = getFollowedList(userId);
            break;
         }
         
         case CommandType.GETFOLLOWERLIST : {
            reply = getFollowerList(userId);
            break;
         }
         
         case CommandType.GETPOSTS : {
            reply = getPosts(userId);
            break;
         }
         
         default:
            break;            
      }

      return reply;
   }
   
   // We assume that this method is called in every replica, so no need to intercept method execution here
   Message createUser(int userId) {
      log.info("Creating user {}.", userId);
      
      User newUser = new User(userId);
      newUser.setId(userId, 0, 0);
      return new Message("OK");
   }

   Message post(int userId, List<Integer> sentFollowers, Post post) {
      log.info("User {} posting content: {}.", userId, post);
      // System.out.println(String.format("User %d materializing content for all their followers: %s.", userId, post));
      
      User poster = User.getUser(userId);
      UserList currentFollowers = poster.getFollowerList();
      List<Integer> currentFollowersIds = currentFollowers.getRawList();
      List<Partition> currentFollowerPartitions = ChirperOracle.getInstance().mapUserIdListToPartitionList(currentFollowersIds);
      List<Partition> commandPartitions = ChirperOracle.getInstance().mapUserIdListToPartitionList(sentFollowers);
      
      String status;
      List<Integer> followersToAdd;
      List<Integer> followersToRemove;
      
      if (poster.posts.containsPost(post)) {
         status = "OK";
      }
      else if (commandPartitions.containsAll(currentFollowerPartitions)) {
         poster.post(post);
         for (int followerId : currentFollowersIds) {
            User follower = User.getUser(followerId);
            follower.addToMaterializedTimeline(post);
         }
         status = "OK";
      }
      else {
         status = "RETRY";
      }
      
      followersToAdd = new ArrayList<Integer>(currentFollowersIds);
      followersToAdd.removeAll(sentFollowers);
      
      followersToRemove = new ArrayList<Integer>(sentFollowers);
      followersToRemove.removeAll(currentFollowersIds);

      // System.out.println("post status: " + status);
      
      return new Message(status, followersToAdd, followersToRemove);
   }

   Message follow(int followerId, int followedId) {
      log.info("User {} now following user {}.", followerId, followedId);
      // System.out.println(String.format("User %d now following user %d", followerId, followedId));

      User followed = User.getUser(followedId);
      User follower = User.getUser(followerId);
      followed.addFollower(followerId);
      follower.follow(followedId);
      follower.addToMaterializedTimeline(followed.getUserPosts());

      return new Message("OK");
   }
   
   @SuppressWarnings("unused")
   Message follow_nop(int followerId, int followedId) {
      log.info("User {} now following user {}.", followerId, followedId);
      // System.out.println(String.format("User %d now following user %d (NOP really...)", followerId, followedId));

      User followed = User.getUser(followedId);
//      followed.addFollower(followerId);
      User follower = User.getUser(followerId);
//      follower.follow(followedId);
//      follower.addToMaterializedTimeline(followed.getUserPosts());
      // just do the remote read:
      followed.getUserPosts();

//      follower.
      return new Message("OK");
   }
   
   Message unfollow(int followerId, int followedId) {
      log.info("User {} now unfollowing user {}.", followerId, followedId);
      // System.out.println(String.format("User %d no longer following user %d", followerId, followedId));
      
      User follower = User.getUser(followerId);      
      boolean didFollow = follower.unfollow(followedId);
      if (didFollow)
         follower.getMaterializedTimeline().removePostsFromUser(followedId);
      
      return new Message("OK");
   }
   
   @SuppressWarnings("unused")
   Message unfollow_nop(int followerId, int followedId) {
      log.info("User {} now unfollowing user {}.", followerId, followedId);
      // System.out.println(String.format("User %d no longer following user %d (NOP really...)", followerId, followedId));
      
      User follower = User.getUser(followerId);      
//      boolean didFollow = follower.unfollow(followedId);
//      if (didFollow)
//         follower.getMaterializedTimeline().removePostsFromUser(followedId);
      return new Message("OK");
   }

   Message getFollowedList(int userId) {
      log.info("User {} requesting their list of followeds.", userId);
      
      return new Message(User.getUser(userId).getFollowedList().getRawList());
   }
   
   Message getFollowerList(int userId) {
      log.info("User {} requesting their list of followers.", userId);
      
      return new Message(User.getUser(userId).getFollowerList().getRawList());
   }
   
   Message getPosts(int userId) {
      log.info("User {} requesting their list of posts.", userId);
      
      return new Message(User.getUser(userId).getUserPosts().getRawList());
   }
   
   Message getTimeline(int userId) {
      log.info("Getting timeline for user {}.", userId);
      // System.out.println(String.format("Getting timeline for user %d", userId));
      
      User user = User.getUser(userId);
      List<Post> timeLine = user.getMaterializedTimeline().getRawList();
      
      return new Message(timeLine);
   }
   
   public ChirperServer(int serverId, int partitionId, String systemConfig, String partitionsConfig, PartitioningOracle oracle, boolean optimisticExecution, boolean exchangesOptimisticState) {
//      super(serverId, partitionId, systemConfig, partitionsConfig, oracle, exchangesOptimisticState);
      super(serverId, partitionId, systemConfig, partitionsConfig, oracle, optimisticExecution);
      LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
      Configuration config = ctx.getConfiguration();
      LoggerConfig loggerConfig = config.getLoggerConfig(ChirperServer.class.toString());
      loggerConfig.setLevel(Level.ERROR);      
   }

   public void createBenchmarkUsers(int numUsers, int msgSize) {
      ChirperOracle oracle = ChirperOracle.getInstance();
      int[] partitionObjectCount = new int[Partition.getPartitionsCount() + 1];
      for (int i = 0 ; i < numUsers ; i++) {
         createUser(i);
         Partition p = oracle.mapUserIdToPartition(i);
         partitionObjectCount[p.getId()]++;
      }
      
      for (int i = 1 ; i <= Partition.getPartitionsCount() ; i++) {
         // System.out.println(partitionObjectCount[i] + " users in partition " + i);
      }
   }
   
   public void loadSocialNetwork(String socialNetworkFilename) {
      List<RandomUser> socialNetwork = Util.loadSocialNetwork(socialNetworkFilename);
      int[] partitionObjectCount = new int[Partition.getPartitionsCount() + 1];
      for (RandomUser user : socialNetwork) {
         createUser(user.userId);
         partitionObjectCount[user.partitionId]++;
      }
      for (RandomUser user : socialNetwork) {

         // conservative user
         User chirperUserCons = User.getUser(user.userId);
         Post postCons = new Post(-1, user.userId, -1, 1416409000000L + user.userId, ("" + user.userId).getBytes());
         chirperUserCons.posts.addPost(postCons);

         // optimistic user
         User chirperUserOpt = (User) User.getUser(user.userId).getOptimisticState();
         Post postOpt  = new Post(-1, user.userId, -1, 1416409000000L + user.userId, ("" + user.userId).getBytes());
         chirperUserOpt.posts.addPost(postOpt);

         for (int followerId : user.followers) {
            // conservative users
            chirperUserCons.followers.people.add(followerId);
            User followerCons = User.getUser(followerId);
            followerCons.followeds.people.add(user.userId);
            followerCons.materializedTimeline.addPost(postCons);

            // optimistic users
            chirperUserOpt.followers.people.add(followerId);
            User followerOpt = (User) User.getUser(followerId).getOptimisticState();
            followerOpt.followeds.people.add(user.userId);
            followerOpt.materializedTimeline.addPost(postOpt);
         }
      }
   }

   public static void main(String[] args) {
      log.info("Entering main().");
      
      if (args.length != 7) {
         System.err.println("usage: serverId partitionId systemConfigFile partitioningFile socialNetworkFile bitOptimisticExecution bitExchangesOptimisticState");
         System.exit(1);
      }
      
      int    serverId = Integer.parseInt(args[0]);
      int    partitionId = Integer.parseInt(args[1]);
      String systemConfigFile = args[2];
      String partitionsConfigFile = args[3];
      String socialNetworkFile = args[4];
      boolean optimisticExecution = Integer.parseInt(args[5]) != 0;
      boolean exchangesOptimisticState = Integer.parseInt(args[6]) != 0;
      
      ChirperServer chirperServer = new ChirperServer(serverId, partitionId, systemConfigFile, partitionsConfigFile, ChirperOracle.getInstance(), optimisticExecution, exchangesOptimisticState);
      
      chirperServer.loadSocialNetwork(socialNetworkFile);
      
      chirperServer.runStateMachine();
   }
}
