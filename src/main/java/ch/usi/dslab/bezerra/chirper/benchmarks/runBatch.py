#!/usr/bin/python

import benchCommon
import math
import time

def estimateBatchTime(algs, parts, wls, min_cli, max_cli, inc_factor, inc_parcel, last_run_time, already_run):
    numLoads = benchCommon.getNumLoads(min_cli, max_cli, inc_factor, inc_parcel)
    retwisRuns = 0 if "RETWIS" not in algs else len(wls) * numLoads
    chirperRuns = 0 if "CHIRPER" not in algs else len(parts) * len(wls) * numLoads
    totalRuns = retwisRuns + chirperRuns - already_run
    return totalRuns * last_run_time

wp = 0
wf = 1
wu = 2
wtl = 3
mix = 4
purepost = 5
purefollow = 6
puretl = 7

workloads = {"mix"            : {wp: 7.5, wf: 3.75, wu: 3.75, wtl:  85},
             #"purepost"       : {wp: 1.0, wf:    0, wu:    0, wtl:   0},
             #"followunfollow" : {wp:   0, wf:  1.0, wu:  1.0, wtl:   0},
             "puretimeline"   : {wp:   0, wf:    0, wu:    0, wtl: 1.0},
             }

#algorithms = ["CHIRPERSSMR", "CHIRPERBASELINE", "CHIRPERPREFETCH", "RETWIS"]
# algorithms = ["RETWIS"]
# both ssmr_no_prefetch and ssmr_prefetch are run with CHIRPERSSMR (this is a mess...)
algorithms = ["CHIRPERSSMR"]#, "CHIRPERBASELINE"]
flagFullOpt = 1
minClients = 1
maxClients = 33
incFactor = 1.2
incParcel = 0

numUsers = 100000

benchCommon.localcmd(benchCommon.cleaner)
benchCommon.localcmd(benchCommon.systemParamSetter)
benchCommon.localcmd(benchCommon.clockSynchronizer)
time.sleep(10)

alreadyRan = 0;
lastRunTime = 120 # estimating each run will take 2 min (120 seconds)
startTime = time.time()

for algorithm in algorithms :

    partitionings = [1] if algorithm == "RETWIS" else [2, 4, 8]
    #partitionings = [1] if algorithm == "RETWIS" else [1]

        
    for numPartitions in partitionings :
        
        socialNetworkFile = benchCommon.socialNetworkDir + "users_" + str(numUsers) + "_partitions_" + str(numPartitions) + ".json"
        
        for wl in workloads :
        
            # ####################################    
            # WORKLOAD
            weightPost        = workloads[wl][wp]
            weightFollow      = workloads[wl][wf]
            weightUnfollow    = workloads[wl][wu]
            weightGetTimeline = workloads[wl][wtl]
            workloadName      = wl
            # ####################################
    

            '''
            if numPartitions == 1 :
                if wl == "purepost" :
                    numClients = 1
                if wl == "puretimeline" :
                    numClients = 1
                if wl == "mix" :
                    numClients = 1
                if wl == "followunfollow" :
                    pass

            if numPartitions == 2 :
                if wl == "purepost" :
                    maxClients = 0
                if wl == "puretimeline" :
                    minClients = 70
                    maxClients = 150
                if wl == "mix" :
                    maxClients = 0
                if wl == "followunfollow" :
                    maxClients = 0

            if numPartitions == 4 :
                if wl == "purepost" :
                    maxClients = 0
                if wl == "puretimeline" :
                    minClients = 70
                    maxClients = 1
                if wl == "mix" :
                    minClients = 80
                    maxClients = 180
                if wl == "followunfollow" :
                    minClients = 70
                    maxClients = 200
            
            if numPartitions == 8 :
                if wl == "purepost" :
                    maxClients = 0
                if wl == "puretimeline" :
                    minClients = 70
                    maxClients = 250
                if wl == "mix" :
                    minClients = 70
                    maxClients = 150
                if wl == "followunfollow" :
                    minClients = 70
                    maxClients = 300
            '''                    
            
            numClients = minClients

            while numClients <= maxClients :
                # usage: onceRunner algorithm(CHIRPERSSMR/.../RETWIS) numClients numPartitions socialNetworkFile [wp wf wu wtl]"
                now = time.time()
                
                remainingTime = estimateBatchTime(algorithms, partitionings, workloads, minClients, maxClients, incFactor, incParcel, lastRunTime, alreadyRan)
                remainingTimeMessage = "[" + time.strftime("%H:%M", time.localtime(now)) + "] remaining time for batch: " + time.strftime("%H:%M", time.gmtime(remainingTime)) + "; expected to finish: " + time.strftime("%H:%M", time.localtime(now+remainingTime))
                print remainingTimeMessage

                experimentCmd = ' '.join([str(val) for val in [benchCommon.onceRunner, algorithm, numClients, numPartitions, socialNetworkFile, weightPost, weightFollow, weightUnfollow, weightGetTimeline, workloadName, flagFullOpt]])
                print experimentCmd
                benchCommon.localcmd(experimentCmd)
                #benchCommon.localcmd(benchCommon.cleaner)

                now = time.time()
                lastRunTime = now - startTime
                startTime = now
                alreadyRan += 1
                
                # increase load
                numClients  = int(math.ceil(numClients * incFactor) + incParcel)
