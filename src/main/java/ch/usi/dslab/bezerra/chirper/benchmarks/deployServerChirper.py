#!/usr/bin/python

import inspect
import os
import sys

import benchCommon
from benchCommon import sarg
from benchCommon import iarg

def script_dir():
#    returns the module path without the use of __file__.  Requires a function defined 
#    locally in the module.
#    from http://stackoverflow.com/questions/729583/getting-file-path-of-imported-module
    return os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None)))

if len(sys.argv) != 8 :
    print "usage: " + sarg(0) + " <server_id> <partition_id> <config_file> <partitioning_file> <social_network_file> <bit_opt_exec(1/0)> <bit_full_opt(1/0)>"
    sys.exit(1)

server_id = iarg(1)
partition_id = iarg(2)
config_file = sarg(3)
partitioning_file = sarg(4)
social_network_file = sarg(5)
bitOptExec = iarg(6)
bitFullOpt = iarg(7)

# debug_port = str(40000 + int(server_id))
# debug_server_str = " -agentlib:jdwp=transport=dt_socket,address=127.0.0.1:" + debug_port + ",server=y,suspend=n "

#   int    serverId = Integer.parseInt(args[0]);
#   int    partitionId = Integer.parseInt(args[1]);
#   String systemConfigFile = args[2];
#   String partitionsConfigFile = args[3];
#   String socialNetworkFile = args[4];

serverCmdPieces  = [benchCommon.javabin, benchCommon.javacp, benchCommon.javaChirperServerClass, server_id]
serverCmdPieces += [partition_id, config_file, partitioning_file, social_network_file, bitOptExec, bitFullOpt]
serverCmdString = " ".join([str(val) for val in serverCmdPieces])
benchCommon.localcmdbg(serverCmdString)

