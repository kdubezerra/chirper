#!/usr/bin/python

import benchCommon
import sys
import systemConfigurer
from benchCommon import sarg, localcmdbg
from benchCommon import iarg
from benchCommon import farg
from os.path import abspath
import time

# usage
def usage() :
    print "usage: " + sarg(0) + " algorithm(RETWIS/CHIRPERSSMR/CHIRPERBASELINE/CHIRPERPREFETCH) numClients numPartitions socialNetworkFile [wp wf wu wtl name] [bitFullOpt(1/0)]"
    sys.exit(1)
    
if (len(sys.argv) not in [5,6,10,11]) :
    usage()

# runAll RETWIS Xclients 

# parameters
algorithm = sarg(1)
numClients = iarg(2)
numPartitions = iarg(3)
socialNetworkFile = abspath(sarg(4))
clientNodes = None
numServers = 0
numCoordinators = 0
numAcceptors = 0
bitFullOpt = None
if (len(sys.argv) in [5,6]) :
    weightPost        = benchCommon.weightPost
    weightFollow      = benchCommon.weightFollow
    weightUnfollow    = benchCommon.weightUnfollow
    weightGetTimeline = benchCommon.weightGetTimeline
    workloadName      = benchCommon.workloadName
    if (len(sys.argv) == 6) :
        bitFullOpt = iarg(5)
elif (len(sys.argv) in [10,11]) :
    weightPost        = farg(5)
    weightFollow      = farg(6)
    weightUnfollow    = farg(7)
    weightGetTimeline = farg(8)
    workloadName      = sarg(9)
    if (len(sys.argv) == 11) :
        bitFullOpt = iarg(10)
else :
    usage()

benchCommon.localcmd(benchCommon.cleaner)
#time.sleep(5)

retwisServerNode = None
chirperSysConfig = None
serverList = None
coordinators = None
acceptors = None
chirperSysConfigFile = None
chirperPartitioningFile = None
gathererNode = None
minClientId = None

#benchCommon.localcmd(benchCommon.systemParamSetter)
optimistic = algorithm in ["CHIRPERBASELINE","CHIRPERPREFETCH"]
bitOptExec = 1 if optimistic else 0
if algorithm == "RETWIS" :
    gathererNode = systemConfigurer.getGathererNode()
    retwisServerNode = systemConfigurer.getRetwisServerNode()
    clientNodes = systemConfigurer.getClientNodes(algorithm)
elif "CHIRPER" in algorithm :
#     configuration = {"config_file":,
#                      "partitioning_file":,
#                      "coordinator_list":,
#                      "acceptor_list":,
#                      "server_list":,
#                      "client_initial_pid":,
#                      "screen_node":,
#                      "gatherer_node":,}
#     benchCommon.localcmd(benchCommon.clockSynchronizer)
    for node in benchCommon.getNonScreenNodes() :
        benchCommon.sshcmdbg(node, benchCommon.continousClockSynchronizer)
    chirperSysConfig = systemConfigurer.generateSystemConfiguration(numPartitions, optimistic)
    time.sleep(10)
    serverList = chirperSysConfig["server_list"]
    coordinators = chirperSysConfig["coordinator_list"]
    acceptors = chirperSysConfig["acceptor_list"]
    numServers = len(serverList)
    numCoordinators = len(coordinators)
    numAcceptors = len(acceptors)
    chirperSysConfigFile = chirperSysConfig["config_file"]
    chirperPartitioningFile = chirperSysConfig["partitioning_file"]
    gathererNode = chirperSysConfig["gatherer_node"]
    minClientId = chirperSysConfig["client_initial_pid"]
    clientNodes = chirperSysConfig["remaining_nodes"]
    

# logfolder
logFolder = ""
if (algorithm == "RETWIS") :
    numServers = 1
    logFolder = "_".join([str(val) for val in [algorithm, workloadName, "clients", numClients, "permits", benchCommon.numPermits, "post", weightPost, "follow", weightFollow, "unfollow", weightUnfollow, "timeline", weightGetTimeline]])
elif ("CHIRPER" in algorithm) :
    algVersionName = "baseline" if bitFullOpt == 0 else "fullopt"
    logFolder = "_".join([str(val) for val in [algorithm, workloadName, "clients", numClients, "permits", benchCommon.numPermits, "partitions", numPartitions, "post", weightPost, "follow", weightFollow, "unfollow", weightUnfollow, "timeline", weightGetTimeline, algVersionName]])





###########################################################################
###########################################################################
# LAUNCH SERVERS
###########################################################################

# launch server(s)
if (algorithm == "RETWIS") :
    benchCommon.sshcmd(retwisServerNode, "mkdir -p /tmp/retwis-db")
    retwisServerCmdPieces = [benchCommon.retwisServerDeployer, retwisServerNode, benchCommon.retwisServerPort]
    retwisServerCmd = " ".join([str(val) for val in retwisServerCmdPieces])
    benchCommon.localcmd(retwisServerCmd)
elif ("CHIRPER" in algorithm) :
    # launch multicast infrastructure
    ridgeCmdPieces = [benchCommon.multicastDeployer, chirperSysConfigFile]
    ridgeCmdString = " ".join(ridgeCmdPieces)
    localcmdbg(ridgeCmdString)
    # launch replicas
    # usage: chirperServerDeployer <server_id> <partition_id> <config_file> <partitioning_file> <social_network_file>"
    for server in serverList :
        sid   = server["id"]
        snode = server["host"]
        spart = server["partition"]
        # deployer <server_id> <partition_id> <config_file> <partitioning_file>"
        chirperServerCmdPieces = [benchCommon.chirperServerDeployer, sid, spart, chirperSysConfigFile, chirperPartitioningFile, socialNetworkFile, bitOptExec, bitFullOpt]
        chirperServerCmdString = " ".join([str(val) for val in chirperServerCmdPieces])
        benchCommon.sshcmdbg(snode, chirperServerCmdString)

time.sleep(4*numPartitions + 10)
print "Waiting for servers to be ready..."
time.sleep(4*numPartitions + 10)
print "Servers should be ready."





###########################################################################
###########################################################################
# LAUNCH CLIENTS
###########################################################################

# launch clients
# deployTestRunners.py: socialNetworkFile numClients weightPost weightFollow weightUnfollow weightGetTimeline algorithm [(for chirper:) numPartitions minClientId configFile partsFile]" 
if (algorithm == "RETWIS") :
    retwisClientsCmdPieces = [benchCommon.clientDeployer, socialNetworkFile, numClients, weightPost, weightFollow, weightUnfollow, weightGetTimeline, algorithm]
    retwisClientsCmd = " ".join([str(val) for val in retwisClientsCmdPieces])
    benchCommon.localcmd(retwisClientsCmd)
elif ("CHIRPER" in algorithm) :
    chirperClientCmdPieces = [benchCommon.clientDeployer, socialNetworkFile, numClients, weightPost, weightFollow, weightUnfollow, weightGetTimeline, algorithm, numPartitions, minClientId, chirperSysConfigFile, chirperPartitioningFile]
    chirperClientCmdString = " ".join([str(val) for val in chirperClientCmdPieces])
    benchCommon.localcmd(chirperClientCmdString)





###########################################################################
###########################################################################
# LAUNCH ACTIVE MONITORS
###########################################################################

# launch active monitors
if (algorithm == "RETWIS") :
    # server bw and cpu monitors
    bwmCmdPieces = [benchCommon.javabin, benchCommon.javacp, benchCommon.javaBWMonitorClass, "server", 0, gathererNode, benchCommon.gathererPort, benchCommon.duration, benchCommon.nonclilogdirRetwis]
    bwmCmdString = " ".join([str(val) for val in bwmCmdPieces])
    retwisServerNode = systemConfigurer.getRetwisServerNode()
    benchCommon.sshcmdbg(retwisServerNode, bwmCmdString)
    cpumCmdPieces = [benchCommon.javabin, benchCommon.javacp, benchCommon.javaCPUMonitorClass, "server", 0, gathererNode, benchCommon.gathererPort, benchCommon.duration, benchCommon.nonclilogdirRetwis]
    cpumCmdString = " ".join([str(val) for val in cpumCmdPieces])
    benchCommon.sshcmdbg(retwisServerNode, cpumCmdString)        
    # client bw and cpu monitors
    clientNodeMap = benchCommon.mapClientsToNodes(numClients, clientNodes)
    cliNodeId = 0
    for node in clientNodes :
        if benchCommon.clientNodeIsEmpty(node, clientNodeMap) :
            continue
        bwmCmdPieces = [benchCommon.javabin, benchCommon.javacp, benchCommon.javaBWMonitorClass, "client_node", cliNodeId, gathererNode, benchCommon.gathererPort, benchCommon.duration, benchCommon.clilogdirRetwis]
        bwmCmdString = " ".join([str(val) for val in bwmCmdPieces])
        benchCommon.sshcmdbg(node, bwmCmdString)
        cpumCmdPieces = [benchCommon.javabin, benchCommon.javacp, benchCommon.javaCPUMonitorClass, "client_node", cliNodeId, gathererNode, benchCommon.gathererPort, benchCommon.duration, benchCommon.clilogdirRetwis]
        cpumCmdString = " ".join([str(val) for val in cpumCmdPieces])
        benchCommon.sshcmdbg(node, cpumCmdString)
        cliNodeId += 1
elif ("CHIRPER" in algorithm) :
    # server bw and cpu monitors
    for nonClient in serverList + coordinators + acceptors :
        bwmCmdPieces =  [benchCommon.javabin, benchCommon.javacp, benchCommon.javaBWMonitorClass , nonClient["role"], nonClient["pid"], chirperSysConfig["gatherer_node"], benchCommon.gathererPort, benchCommon.duration, benchCommon.nonclilogdirChirper]
        bwmCmdString = " ".join([str(val) for val in bwmCmdPieces])
        benchCommon.sshcmdbg(nonClient["host"], bwmCmdString)
        cpumCmdPieces = [benchCommon.javabin, benchCommon.javacp, benchCommon.javaCPUMonitorClass, nonClient["role"], nonClient["pid"], chirperSysConfig["gatherer_node"], benchCommon.gathererPort, benchCommon.duration, benchCommon.nonclilogdirChirper]
        cpumCmdString = " ".join([str(val) for val in cpumCmdPieces])
        benchCommon.sshcmdbg(nonClient["host"], cpumCmdString)        
    # client bw and cpu monitors
    clientNodeMap = benchCommon.mapClientsToNodes(numClients, clientNodes)
    cliNodeId = minClientId
    for node in clientNodes :
        if benchCommon.clientNodeIsEmpty(node, clientNodeMap) :
            continue
        #bwmCmdPieces = [benchCommon.javabin, benchCommon.javacp, benchCommon.javaBWMonitorClass, "client_node", cliNodeId, gathererNode, benchCommon.gathererPort, benchCommon.duration, benchCommon.clilogdirChirper]
        #bwmCmdString = " ".join([str(val) for val in bwmCmdPieces])
        #benchCommon.sshcmdbg(node, bwmCmdString)
        cpumCmdPieces = [benchCommon.javabin, benchCommon.javacp, benchCommon.javaCPUMonitorClass, "client_node", cliNodeId, gathererNode, benchCommon.gathererPort, benchCommon.duration, benchCommon.clilogdirChirper]
        cpumCmdString = " ".join([str(val) for val in cpumCmdPieces])
        benchCommon.sshcmdbg(node, cpumCmdString)
        cliNodeId += 1





###########################################################################
###########################################################################
# LAUNCH GATHERER
###########################################################################

# launch gatherer last and wait for it to finish
# wait for gatherer with timeout; if timeout, kill everything, including Redis
gathererCmdPieces = [benchCommon.gathererDeployer, gathererNode, algorithm, numPartitions, numClients, numServers, numCoordinators, numAcceptors, logFolder]
gathererCmd = " ".join([str(val) for val in gathererCmdPieces])
print gathererCmd


fullLogDir = benchCommon.gathererBaseLogDir + logFolder + "/"

benchCommon.localcmd(gathererCmd)
benchCommon.localcmd("cp " + benchCommon.benchCommonPath + " " + fullLogDir)
if "CHIRPER" in algorithm :
    benchCommon.localcmd("cp " + benchCommon.sysConfigFile + " " + fullLogDir)
    benchCommon.localcmd("cp " + benchCommon.partitionsFile + " " + fullLogDir)
elif algorithm == "RETWIS" :
    benchCommon.sshcmd(retwisServerNode, "rm -rf /tmp/retwis-db")
benchCommon.localcmd(benchCommon.cleaner)

