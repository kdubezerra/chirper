/*

 Chirper - A twitter clone based on Eyrie 2 (Optimistic S-SMR)
 Copyright (C) 2014, University of Lugano
 
 This file is part of Chirper.
 
 Chirper is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.chirper.benchmarks;

import ch.usi.dslab.bezerra.chirper.ChirperClient;
import ch.usi.dslab.bezerra.chirper.Post;
import ch.usi.dslab.bezerra.eyrie.cs.CallbackHandler;

public class ChirperBenchClient implements ClientInterface {
   ChirperClient chirperClient;
   
   public ChirperBenchClient(ChirperClient client) {
      chirperClient = client;
   }
   
   public void loadSocialNetworkIntoCache(String file) {
      chirperClient.loadSocialNetworkIntoCache(file);
   }

   @Override
   public void createUser(int userId, CallbackHandler optimisticHandler, CallbackHandler conservativeHandler, Object context) {
      chirperClient.createUserNB(userId, optimisticHandler, conservativeHandler, context);
   }

   @Override
   public void post(int userId, byte[] post, CallbackHandler optimisticHandler, CallbackHandler conservativeHandler, Object context) {
      Post postObj = new Post(chirperClient.getId(), userId, System.currentTimeMillis(), post);
      chirperClient.postNB(userId, postObj, optimisticHandler, conservativeHandler, context);
   }

   @Override
   public void follow(int followerId, int followedId, CallbackHandler optimisticHandler, CallbackHandler conservativeHandler, Object context) {
      chirperClient.followNB(followerId, followedId, optimisticHandler, conservativeHandler, context);
   }

   @Override
   public void unfollow(int followerId, int followedId, CallbackHandler optimisticHandler, CallbackHandler conservativeHandler, Object context) {
      chirperClient.unfollowNB(followerId, followedId, optimisticHandler, conservativeHandler, context);
   }

   @Override
   public void getTimeline(int userId, CallbackHandler optimisticHandler, CallbackHandler conservativeHandler, Object context) {
      chirperClient.getTimelineNB(userId, optimisticHandler, conservativeHandler, context);
   }

   @Override
   public void follow_NOP(int followerId, int followedId, CallbackHandler optimisticHandler, CallbackHandler conservativeHandler, Object context) {
      chirperClient.follow_NOP_NB(followerId, followedId, optimisticHandler, conservativeHandler, context);  
   }

   @Override
   public void unfollow_NOP(int followerId, int followedId, CallbackHandler optimisticHandler, CallbackHandler conservativeHandler, Object context) {
      chirperClient.unfollow_NOP_NB(followerId, followedId, optimisticHandler, conservativeHandler, context);
   }

}
