#!/usr/bin/python


import sys
import benchCommon
import systemConfigurer
import time
from benchCommon import sarg
from benchCommon import iarg

def usage() :
    print "usage: " + sys.argv[0] + " gathererHost algorithm(RETWIS/CHIRPERSSMR/CHIRPERBASELINE/CHIRPERPREFETCH numPartitions numClients numServers numCoordinators numAcceptors [logFolder]"
    sys.exit()
    

# command = "java -cp target/libsense-git.jar ch.usi.dslab.bezerra.sense.DataGatherer 60000 /tmp/ latency CLIENT_LATENCY_CONSERVATIVE 1 throughput CLIENT_THROUGHPUT_CONSERVATIVE 1"

if (len(sys.argv) not in range(8,10)) :
    usage()

#parameters
clientNodes = None
gathererHost = sarg(1)
algorithm = sarg(2)
numPartitions = iarg(3)
numClients = iarg(4)
numServers = iarg(5)
numCoordinators = iarg(6)
numAcceptors = sarg(7)
if (len(sys.argv) == 9) :
    logFolder = sarg(8)
elif (len(sys.argv) == 8) :
    logFolder = ""
else :
    usage()

print "algorithm = " + algorithm

modes = None
if algorithm == "RETWIS" :
    modes = ["conservative"]
    clientNodes = systemConfigurer.getClientNodes(algorithm)
elif algorithm in ["CHIRPERSSMR"] :
    modes = ["conservative"]
    clientNodes = systemConfigurer.getClientNodes(algorithm, numPartitions)
elif algorithm in ["CHIRPERBASELINE", "CHIRPERPREFETCH"] :
    modes = ["conservative", "optimistic"]
    clientNodes = systemConfigurer.getClientNodes(algorithm, numPartitions)
    
logsargs = []

# client logs

numUsedClientNodes = benchCommon.numUsedClientNodes(numClients, clientNodes)

clientIndividualLogs = ["latency", "throughput", "timeline"]
clientIndividualSubLogs = ["overall", "post", "follow", "unfollow", "gettimeline"]
if (numClients > 0) :
    for log in clientIndividualLogs :
        for sublog in clientIndividualSubLogs :
            for mode in modes :
                logsargs.append(log)
                logsargs.append('_'.join(["client", mode, sublog]))
                logsargs.append(str(numUsedClientNodes))

if "optimistic" in modes :
    logsargs.append("mistakes")
    logsargs.append("client_optimistic")
    logsargs.append(str(numUsedClientNodes))

clientNodeLogs = ["cpu"]#, "bandwidth"]
if (numClients > 0) :
    for log in clientNodeLogs :
        for mode in modes :
            logsargs.append(log)
            logsargs.append("client_node")
            logsargs.append(str(numUsedClientNodes))
            
# server logs
serverLogs = ["cpu","bandwidth"]
if (numServers > 0) :
    for log in serverLogs :
        logsargs.append(log)
        logsargs.append("server")
        logsargs.append(str(numServers))

# ensemble nodes
# coordinator logs
coordinatorLogs = ["cpu", "bandwidth"]
if (numCoordinators > 0) :
    for log in coordinatorLogs :
        logsargs.append(log)
        logsargs.append("coordinator")
        logsargs.append(str(numCoordinators))

# acceptor logs
acceptorLogs = ["cpu", "bandwidth"]
if (numAcceptors > 0) :
    for log in acceptorLogs :
        logsargs.append(log)
        logsargs.append("acceptor")
        logsargs.append(str(numAcceptors))
        
# based on algorithm, build list of monitors
# for iargs in range(len(logsargs), 3) :
#     logtype = logsargs[iargs]
#     nodetype = logsargs[iargs + 1]
#     count = int(logsargs[iargs+2])


logPath = benchCommon.gathererBaseLogDir + logFolder
cmdArgs  = [benchCommon.javabin, benchCommon.javacp, benchCommon.javaGathererClass, benchCommon.gathererPort, logPath] + logsargs
cmdString = ' '.join(cmdArgs);
print cmdString


timetowait = int(benchCommon.duration) * 5
 
exitcode = benchCommon.sshcmd(gathererHost, cmdString, timetowait)
if exitcode != 0 :
    benchCommon.localcmd("touch %s/FAILED.txt" % (logPath))
     
benchCommon.localcmd(benchCommon.cleaner)
time.sleep(10)
