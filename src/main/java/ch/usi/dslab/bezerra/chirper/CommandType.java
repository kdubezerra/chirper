/*

 Chirper - A twitter clone based on Eyrie 2 (Optimistic S-SMR)
 Copyright (C) 2014, University of Lugano
 
 This file is part of Chirper.
 
 Chirper is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.chirper;

public interface CommandType {
   public static final int NEWUSER = 0;
   public static final int POST = 1;
   public static final int FOLLOW = 3;
   public static final int UNFOLLOW = 4;
   public static final int GETTIMELINE = 5;
   public static final int GETFOLLOWEDLIST = 6;
   public static final int GETFOLLOWERLIST = 7;
   public static final int GETPOSTS = 8;
   public static final int FOLLOW_NOP = 9;
   public static final int UNFOLLOW_NOP = 10;
}
