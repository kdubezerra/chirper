/*

 Chirper - A twitter clone based on Eyrie 2 (Optimistic S-SMR)
 Copyright (C) 2014, University of Lugano
 
 This file is part of Chirper.
 
 Chirper is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.chirper.benchmarks;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicLong;

import ch.usi.dslab.bezerra.chirper.ChirperClient;
import ch.usi.dslab.bezerra.chirper.CommandType;
import ch.usi.dslab.bezerra.chirper.Post;
import ch.usi.dslab.bezerra.chirper.PostsList;
import ch.usi.dslab.bezerra.chirper.util.RicardoNetworkGenerator.RandomUser;
import ch.usi.dslab.bezerra.chirper.util.Util;
import ch.usi.dslab.bezerra.eyrie.cs.CallbackHandler;
import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.bezerra.sense.DataGatherer;
import ch.usi.dslab.bezerra.sense.monitors.LatencyPassiveMonitor;
import ch.usi.dslab.bezerra.sense.monitors.MistakeRatePassiveMonitor;
import ch.usi.dslab.bezerra.sense.monitors.ThroughputPassiveMonitor;
import ch.usi.dslab.bezerra.sense.monitors.TimelinePassiveMonitor;

public class TestRunner {
   
   public static class BenchCallbackContext {
      private static AtomicLong lastReqId = new AtomicLong();
      long startTimeNano;
      long timelineBegin;
      int  commandType;
      long reqId;
      public BenchCallbackContext(int commandType) {
         this.startTimeNano = System.nanoTime();
         this.timelineBegin = System.currentTimeMillis();
         this.commandType   = commandType;
         this.reqId = lastReqId.incrementAndGet();
      }
      public BenchCallbackContext(long startTimeNano, long timelineBegin, int commandType) {
         this.startTimeNano = startTimeNano;
         this.timelineBegin = timelineBegin;
         this.commandType   = commandType;
         this.reqId = lastReqId.incrementAndGet();
      }
   }
   
   public static class BenchCallbackHandler implements CallbackHandler {
      TestRunner parentRunner;
      boolean conservative;
      private LatencyPassiveMonitor    overallLatencyMonitor   , postLatencyMonitor   , followLatencyMonitor   , unfollowLatencyMonitor   , getTimelineLatencyMonitor;
      private ThroughputPassiveMonitor overallThroughputMonitor, postThroughputMonitor, followThroughputMonitor, unfollowThroughputMonitor, getTimelineThroughputMonitor;
      private TimelinePassiveMonitor   overallTimelineMonitor  , postTimelineMonitor  , followTimelineMonitor  , unfollowTimelineMonitor  , getTimelineTimelineMonitor;
      private OptimisticReplyVerifier  replyVerifier;
      
      public BenchCallbackHandler(TestRunner parentRunner, int clientId, boolean conservative, OptimisticReplyVerifier replyVerifier) {
         String client_mode = "client_" + (conservative ? "conservative" : "optimistic");
         this.parentRunner = parentRunner;
         this.conservative = conservative;
         this.replyVerifier = replyVerifier;
         
         // Latency monitors
         overallLatencyMonitor     = new LatencyPassiveMonitor(clientId, client_mode + "_overall");
         postLatencyMonitor        = new LatencyPassiveMonitor(clientId, client_mode + "_post",        false);
         followLatencyMonitor      = new LatencyPassiveMonitor(clientId, client_mode + "_follow",      false);
         unfollowLatencyMonitor    = new LatencyPassiveMonitor(clientId, client_mode + "_unfollow",    false);
         getTimelineLatencyMonitor = new LatencyPassiveMonitor(clientId, client_mode + "_gettimeline", false);
         
         // Throughput monitors
         overallThroughputMonitor     = new ThroughputPassiveMonitor(clientId, client_mode + "_overall");
         postThroughputMonitor        = new ThroughputPassiveMonitor(clientId, client_mode + "_post",        false);
         followThroughputMonitor      = new ThroughputPassiveMonitor(clientId, client_mode + "_follow",      false);
         unfollowThroughputMonitor    = new ThroughputPassiveMonitor(clientId, client_mode + "_unfollow",    false);
         getTimelineThroughputMonitor = new ThroughputPassiveMonitor(clientId, client_mode + "_gettimeline", false);
         
         // Timeline monitor
         overallTimelineMonitor     = new TimelinePassiveMonitor(clientId, client_mode + "_overall");
         postTimelineMonitor        = new TimelinePassiveMonitor(clientId, client_mode + "_post",        false);
         followTimelineMonitor      = new TimelinePassiveMonitor(clientId, client_mode + "_follow",      false);
         unfollowTimelineMonitor    = new TimelinePassiveMonitor(clientId, client_mode + "_unfollow",    false);
         getTimelineTimelineMonitor = new TimelinePassiveMonitor(clientId, client_mode + "_gettimeline", false);

      }
      
      @Override
      public void handleCallback(Object reply, Object context) {
         if (conservative) parentRunner.releasePermit();
         Message replyMsg = (Message) reply;
         BenchCallbackContext benchContext = (BenchCallbackContext) context;
         long nowNano  = System.nanoTime();
         long nowClock = System.currentTimeMillis();
         
         // log latency
         overallLatencyMonitor.logLatency(benchContext.startTimeNano, nowNano);
         overallTimelineMonitor.logTimeline("client_send", benchContext.timelineBegin,
               "learner_delivered", replyMsg.t_learner_delivered,
               "command_dequeued" , replyMsg.t_command_dequeued,
               "client_received"  , nowClock);
         
         // increment throughput count
         overallThroughputMonitor.incrementCount();
         
         LatencyPassiveMonitor    requestLatencyMonitor    = null;
         TimelinePassiveMonitor   requestTimelineMonitor   = null;
         ThroughputPassiveMonitor requestThroughputMonitor = null;
         switch (benchContext.commandType) {
            case CommandType.POST :
               requestLatencyMonitor    = postLatencyMonitor;
               requestTimelineMonitor   = postTimelineMonitor;
               requestThroughputMonitor = postThroughputMonitor;
               break;
            case CommandType.FOLLOW_NOP :
               requestLatencyMonitor    = followLatencyMonitor;
               requestTimelineMonitor   = followTimelineMonitor;
               requestThroughputMonitor = followThroughputMonitor;
               break;
            case CommandType.UNFOLLOW_NOP :
               requestLatencyMonitor    = unfollowLatencyMonitor;
               requestTimelineMonitor   = unfollowTimelineMonitor;
               requestThroughputMonitor = unfollowThroughputMonitor;
               break;
            case CommandType.GETTIMELINE :
               requestLatencyMonitor    = getTimelineLatencyMonitor;
               requestTimelineMonitor   = getTimelineTimelineMonitor;
               requestThroughputMonitor = getTimelineThroughputMonitor;
               break;
            default:
               break;
         }
         requestLatencyMonitor.logLatency(benchContext.startTimeNano, nowNano);
         requestTimelineMonitor.logTimeline("client_send", benchContext.timelineBegin,
               "learner_delivered", replyMsg.t_learner_delivered,
               "command_dequeued" , replyMsg.t_command_dequeued,
               "client_received"  , nowClock);
         requestThroughputMonitor.incrementCount();
         
         if (replyVerifier != null)
            replyVerifier.addReply(benchContext.reqId, benchContext.commandType, reply);
      }
      
   }
   
   public static class OptimisticReplyVerifier {
      Map<Long, Integer> replyHashes;
//      Map<Long, Object> replies;
      MistakeRatePassiveMonitor mrpMon;
      
      public OptimisticReplyVerifier(int clientId){
         replyHashes = new HashMap<Long, Integer>();
//         replies = new HashMap<Long, Object>();
         mrpMon = new MistakeRatePassiveMonitor(clientId, "client_optimistic", true);
      }
      
      @SuppressWarnings("unchecked")
      int hashReply(long reqId, int cmdType, Object reply) {
         int replyHash = 0;
         
         switch (cmdType) {
            case CommandType.POST :
            case CommandType.FOLLOW_NOP :
            case CommandType.UNFOLLOW_NOP :
               replyHash = 0;
            break;
            case CommandType.GETTIMELINE :
               List<Post> postsList = (List<Post>) ((Message) reply).getItem(0);
               replyHash = PostsList.hashPostsList(postsList);
            break;
            default:
               replyHash = 0;
            break;
         }
         
         return replyHash;
      }
      
      public void addReply(long reqId, int cmdType, Object reply) {
         int thisReplyHash = hashReply(reqId, cmdType, reply);
         boolean correct = true;
         boolean matched = false;

         synchronized (replyHashes) {
            if (replyHashes.containsKey(reqId) == false) {
               replyHashes.put(reqId, thisReplyHash);
            }
            else {
               matched = true;
               int storedReplyHash = replyHashes.get(reqId);
               correct = (thisReplyHash == storedReplyHash);
               replyHashes.remove(reqId);
            }
         }
         
         if (matched)
            mrpMon.incrementCounts(1, correct ? 0 : 1);
      }
      
      @SuppressWarnings("unchecked")
      boolean equivalentReplies(int cmdType, Object reply1, Object reply2) {
         boolean equivalent;
         switch (cmdType) {
            case CommandType.POST :
            case CommandType.FOLLOW_NOP :
            case CommandType.UNFOLLOW_NOP :
               equivalent = true;
            break;
            case CommandType.GETTIMELINE :
               Set<Post> posts1 = new HashSet<Post>((List<Post>) ((Message) reply1).getItem(0));
               Set<Post> posts2 = new HashSet<Post>((List<Post>) ((Message) reply2).getItem(0));
               equivalent = posts1.equals(posts2);
            break;
            default:
               equivalent = true;
            break;
         }
         return equivalent;
      }
      
   }
   
   Random randomGenerator;
   
   ClientInterface client;
   Semaphore sendPermits;
   int numUsers;
   
   double totalWeight;
   double postWeight;
   double followWeight;
   double unfollowWeight;
   double getTimelineWeight;
   
   CallbackHandler conservativeCallbackHandler;
   CallbackHandler   optimisticCallbackHandler;
   
   public TestRunner (ClientInterface client, int clientId, int numPermits, String socialNetworkFile,
         double postProbability, double followProbability, double unfollowProbability, double getTimelineProbability,
         boolean hasOptimistic) {
      
      randomGenerator = new Random(System.nanoTime());
      
      this.client = client;
      
      sendPermits = new Semaphore(numPermits);
      List<RandomUser> socialNetwork = Util.loadSocialNetwork(socialNetworkFile);
      this.numUsers = socialNetwork.size();
      
      postWeight = postProbability;
      followWeight = followProbability;
      unfollowWeight = unfollowProbability;
      getTimelineWeight = getTimelineProbability;
      totalWeight = postWeight + followWeight + unfollowWeight + getTimelineWeight;
      
      OptimisticReplyVerifier replyVerifier = hasOptimistic ? new OptimisticReplyVerifier(clientId) : null;
      
      conservativeCallbackHandler  = new BenchCallbackHandler(this, clientId,  true, replyVerifier);
      if (hasOptimistic)
         optimisticCallbackHandler = new BenchCallbackHandler(this, clientId, false, replyVerifier);
   }
   
   void getPermit() {
      try {
         sendPermits.acquire();
      } catch (InterruptedException e) {
         e.printStackTrace();
         System.exit(1);
      }
   }
   
   void releasePermit() {
      sendPermits.release();
   }

   public void sendOneCommand() {
      getPermit();

      double randDouble = randomGenerator.nextDouble() * totalWeight;
      int userId = randomGenerator.nextInt(numUsers);

      if (randDouble <= postWeight) {
         // generate postCommand
         // System.out.println(String.format("TestRunner:: user %d sending command: post(...)", userId));
         byte[] post = new byte[140];
         BenchCallbackContext context = new BenchCallbackContext(CommandType.POST);
         client.post(userId, post, optimisticCallbackHandler, conservativeCallbackHandler, context);
         return;
      }
      randDouble -= postWeight;

      if (randDouble <= followWeight) {
         // generate followCommand
         int followerId = userId;
         int followedId = randomGenerator.nextInt(numUsers);
         // System.out.println(String.format("TestRunner:: user %d sending command: follow_NOP(%d)", followerId, followedId));
         BenchCallbackContext context = new BenchCallbackContext(CommandType.FOLLOW_NOP);
         client.follow_NOP(followerId, followedId, optimisticCallbackHandler, conservativeCallbackHandler, context);
         return;
      }
      randDouble -= followWeight;

      if (randDouble <= unfollowWeight) {
         // generate unfollowCommand
         int followerId = userId;
         int followedId = randomGenerator.nextInt(numUsers);
         // System.out.println(String.format("TestRunner:: user %d sending command: unfollow_NOP(%d)", followerId, followedId));
         BenchCallbackContext context = new BenchCallbackContext(CommandType.UNFOLLOW_NOP);
         client.unfollow_NOP(followerId, followedId, optimisticCallbackHandler, conservativeCallbackHandler, context);
         return;
      }
      randDouble -= unfollowWeight;

      if (randDouble <= getTimelineWeight) {
         // generate getTimelineCommand
         // System.out.println(String.format("TestRunner:: user %d sending command: getTimeline(...)", userId));
         BenchCallbackContext context = new BenchCallbackContext(CommandType.GETTIMELINE);
         client.getTimeline(userId, optimisticCallbackHandler, conservativeCallbackHandler, context);
         return;
      }
   }
   
   public void runTests(int durationSecs) {
      long start = System.currentTimeMillis();
      long end = start + durationSecs * 1000;
      while (System.currentTimeMillis() < end)
         sendOneCommand();
      try {
         Thread.sleep(10 * durationSecs * 1000);
      } catch (InterruptedException e) {
         e.printStackTrace();
         System.exit(1);
      }
   }
   
   
   static ClientInterface getClientInterface(String... args) {
      int clientId = Integer.parseInt(args[0]);
      String implementation = args[1];
      if (implementation.contains("CHIRPER")) {
            String systemConfigFile = args[2];
            String partitioningFile = args[3];
            ChirperClient chirperClient =
                  new ChirperClient(clientId, systemConfigFile, partitioningFile);
            return new ChirperBenchClient(chirperClient);
      }
      else if (implementation.equals("RETWIS")) {
            String redisServerHost = args[2];
            int redisServerPort = Integer.parseInt(args[3]);
            String idMapFile = args[4];
            System.out.println(String.format("Redis server: %s:%d", redisServerHost, redisServerPort));
            RetwisBenchClient retwisClient = RetwisBenchClient.newClientTransactional(redisServerHost, redisServerPort);
            retwisClient.loadIdMapFromFile(idMapFile);
            return retwisClient;
      }
      else if (implementation.equals("BUZZER")) {
         return null;
      }
      return null;
   }
   
   /**
    * @param args
    */
   public static void main(String[] args) {

      int experimentDuration = Integer.parseInt(args[0]);
      String fileDirectory = args[1];
      String gathererAddress = args[2];
      int gathererPort = Integer.parseInt(args[3]);
      int numPermits = Integer.parseInt(args[4]);
      String socialNetworkFile = args[5];
      double postWeight = Double.parseDouble(args[6]);
      double followWeight = Double.parseDouble(args[7]);
      double unfollowWeight = Double.parseDouble(args[8]);
      double getTimelineWeight = Double.parseDouble(args[9]);
      String[] clientArgs = Arrays.copyOfRange(args, 10, args.length);
      int clientId = Integer.parseInt(args[10]);
      String implementation = args[11];
      // include clientId in clientArgs
      ClientInterface client = getClientInterface(clientArgs);
      
      boolean hasOptimistic = implementation.contains("CHIRPER");
//      boolean hasOptimistic = implementation.equals("CHIRPERBASELINE") || implementation.equals("CHIRPERPREFETCH");
      if (client instanceof ChirperBenchClient) {
         ((ChirperBenchClient) client).loadSocialNetworkIntoCache(socialNetworkFile);
      }

      // ====================================
      // setting up monitoring infrastructure
      DataGatherer.configure(experimentDuration, fileDirectory, gathererAddress, gathererPort);
      // ====================================


      TestRunner runner = new TestRunner(client, clientId, numPermits, socialNetworkFile,
            postWeight, followWeight, unfollowWeight, getTimelineWeight, hasOptimistic);

      // actually run (this client's part of) the experiment
      runner.runTests(experimentDuration);

      // wait for the monitors to finish logging and sendind data to the DataGatherer

   }

}
