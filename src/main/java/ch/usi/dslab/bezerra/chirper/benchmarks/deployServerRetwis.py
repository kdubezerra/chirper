#!/usr/bin/python

import benchCommon
from systemConfigurer_ridge import py


serverHost = systemConfigurer_ridge.py.getRetwisServerNode()
serverPort = benchCommon.retwisServerPort

benchCommon.freePort(serverHost, serverPort)
benchCommon.sshcmdbg(serverHost, "redis-server " + str(benchCommon.retwisConfigFile))