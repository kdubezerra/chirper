#!/usr/bin/python



# ============================================
# ============================================
# preamble

import sys
import systemConfigurer
import benchCommon
from benchCommon import sarg, retwisServerPort
from benchCommon import iarg
from benchCommon import farg

def usage() :
    print "usage: " + sarg(0) + " socialNetworkFile numClients weightPost weightFollow weightUnfollow weightGetTimeline algorithm [(for chirper:) numPartitions minClientId configFile partsFile]" 
    sys.exit(1)

if len(sys.argv) not in [8, 12] :
    usage()

# constants
NODE = 0
CLIENTS = 1

# deployment layout
socialNetworkFile = sarg(1)
numClients = iarg(2)

# command arguments
numPermits = benchCommon.numPermits
weightPost = farg(3)
weightFollow = farg(4)
weightUnfollow = farg(5)
weightGetTimeline = farg(6)
algorithm = sarg(7) # RETWIS / CHIRPERSSRM / CHIRPERBASELINE / CHIRPERPREFETCH
algargs = []
gathererNode = systemConfigurer.getGathererNode()
clientNodes = None
numPartitions = None
clientId = None

# chirper extra arguments

    # minClientId
    # configFile
    # partsFile




clientId = None
if (algorithm == "RETWIS") :
    clientNodes = systemConfigurer.getClientNodes(algorithm)
    redisHost = systemConfigurer.getRetwisServerNode()
    redisPort = benchCommon.retwisServerPort
    clientId = 0
    benchCommon.sshcmd(redisHost, "redis-cli -h " + redisHost + " -p " + str(redisPort) + " CONFIG SET appendfsync everysec")
    algargs = [clientId, algorithm, redisHost, redisPort, benchCommon.userIdMapFile]
    cmd  = [benchCommon.javabin, benchCommon.javacp, benchCommon.javaRetwisUserCreatorClass]
    cmd += [socialNetworkFile, redisHost, redisPort, benchCommon.userIdMapFile]
    createUsersCommand = " ".join([str(val) for val in cmd])
    print "Creating users..."
    print createUsersCommand
    benchCommon.sshcmd(redisHost, createUsersCommand)
    benchCommon.sshcmd(redisHost, "redis-cli -h " + redisHost + " -p " + str(redisPort) + " CONFIG SET appendfsync always")
    print "Done."
    
elif (algorithm in ["CHIRPERSSMR", "CHIRPERBASELINE", "CHIRPERPREFETCH"]) :
    # chirper's extra arguments: numPartitions minClientId configFile partsFile
    numPartitions = iarg(8)
    clientId = iarg(9)
    configFile = sarg(10)
    partsFile = sarg(11)
    optimistic = (algorithm != "CHIRPERSSMR")
    chirperConfig = systemConfigurer.generateSystemConfiguration(numPartitions, optimistic, saveToFile = True)
    clientNodes = systemConfigurer.getClientNodes(algorithm, numPartitions)
    algargs = [clientId, algorithm, configFile, partsFile]

# ============================================
# ============================================



# ============================================
# ============================================
# functions

# ============================================
# ============================================



# ============================================
# ============================================
# begin

logDir = benchCommon.clilogdirChirper if "CHIRPER" in algorithm else benchCommon.clilogdirRetwis

# commonargs  = [benchCommon.javabin, benchCommon.javacp, benchCommon.javaRunnerClass]
# commonargs += [benchCommon.duration, logDir, gathererNode, benchCommon.gathererPort]
# commonargs += [numPermits, socialNetworkFile]
# commonargs += [weightPost, weightFollow, weightUnfollow, weightGetTimeline]

clientMapping = benchCommon.mapClientsToNodes(numClients, clientNodes)
for mapping in clientMapping :
#     for i in range(mapping[CLIENTS]) :
        
    if (mapping[CLIENTS] == 0) :
        continue

    numPermits = mapping[CLIENTS]
    
    commonargs  = [benchCommon.javabin, benchCommon.javacp, benchCommon.javaRunnerClass]
    commonargs += [benchCommon.duration, logDir, gathererNode, benchCommon.gathererPort]
    commonargs += [numPermits, socialNetworkFile]
    commonargs += [weightPost, weightFollow, weightUnfollow, weightGetTimeline]
    
    algargs[0] = str(clientId)
    cmdString = " ".join([str(val) for val in (commonargs + algargs)])
    print cmdString
    benchCommon.sshcmdbg(mapping[NODE], cmdString)
    clientId += 1
         

        
        
        
        
        
        
        
        
        
        
        
        
        
        