#!/usr/bin/python

import inspect
import os
import time
import sys

def script_dir():
#    returns the module path without the use of __file__.  Requires a function defined 
#    locally in the module.
#    from http://stackoverflow.com/questions/729583/getting-file-path-of-imported-module
   return os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None)))

if len(sys.argv) != 4 :
    print "usage: " + sys.argv[0] + " <server id> <partition_id> <social_network_file>"
    sys.exit(1)

server_id           = sys.argv[1] + " "
partition           = sys.argv[2] + " "
social_network_file = sys.argv[3] + " "


# starting servers

java_bin = "java -XX:+UseG1GC -cp "
config_file = script_dir() + "/ridge_config.json "
app_classpath = script_dir() + "/../../../../../../../../target/chirper-git.jar "
server_class = "ch.usi.dslab.bezerra.chirper.ChirperServer "
client_class = "ch.usi.dslab.bezerra.chirper.ChirperClient "
partitioning_file = script_dir() + "/partitions.json "
bitOptExecution = "0 "
bitFullOpt = "0 "

debug_port = str(40000 + int(server_id))
debug_server_str = " -agentlib:jdwp=transport=dt_socket,address=127.0.0.1:" + debug_port + ",server=y,suspend=n "

silence_servers = False
#silence_servers = True

# int    serverId = Integer.parseInt(args[0]);
# int    partitionId = Integer.parseInt(args[1]);
# String systemConfigFile = args[2];
# String partitionsConfigFile = args[3];
# String socialNetworkFile = args[4];

server_cmd = java_bin + app_classpath + debug_server_str + server_class + server_id + partition + config_file + partitioning_file + social_network_file + bitOptExecution + bitFullOpt
if silence_servers : server_cmd += " &> /dev/null "
server_cmd += " & "
os.system(server_cmd)
