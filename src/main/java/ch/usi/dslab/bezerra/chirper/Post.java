/*

 Chirper - A twitter clone based on Eyrie 2 (Optimistic S-SMR)
 Copyright (C) 2014, University of Lugano
 
 This file is part of Chirper.
 
 Chirper is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.chirper;

import java.io.Serializable;
import java.sql.Time;
import java.util.Comparator;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import ch.usi.dslab.bezerra.eyrie.ObjId;

public class Post implements Serializable {
   private static final long serialVersionUID = 1L;
   
   public static class PostComparatorNewestFirst implements Comparator<Post> {
      @Override
      public int compare(Post o1, Post o2) {
         return o1.timestamp > o2.timestamp ? -1 : (o1.timestamp == o2.timestamp ? 0 : 1);
      }
   }
   public static PostComparatorNewestFirst comparatorNewestFirst = new PostComparatorNewestFirst();

   private static AtomicInteger lastPostSeq = new AtomicInteger();
   
   ObjId  postId;
   long   timestamp;
   byte[] content;
   
   public Post() {}
   
   public Post(int posterId, long time, byte[] content) {
      Random rand = new Random(System.nanoTime());
      int fakeClientId = rand.nextInt();
      this.postId = new ObjId(fakeClientId, posterId, lastPostSeq.incrementAndGet());
      this.timestamp = time;
      this.content   = content;
   }
   
   public Post(int clientId, int posterId, long time, byte[] content) {
      this.postId = new ObjId(clientId, posterId, lastPostSeq.incrementAndGet());
      this.timestamp = time;
      this.content   = content;
   }
   
   public Post(int clientId, int posterId, int seq, long time, byte[] content) {
      this.postId = new ObjId(clientId, posterId, seq);
      this.timestamp = time;
      this.content   = content;
   }

   public int getPosterId() {
      return postId.second;
   }
   
   @Override
   public boolean equals(Object other) {
      Post otherPost = (Post) other;
      return this.postId.equals(otherPost.postId);
   }
   
   @Override
   public int hashCode() {
      return postId.hashCode();
   }
   
   @Override
   public String toString() {
      Time formattedTime = new Time(timestamp);
      return String.format("%d (@ %s): %s", getPosterId(), formattedTime.toLocalTime(), new String(content));
   }
}
