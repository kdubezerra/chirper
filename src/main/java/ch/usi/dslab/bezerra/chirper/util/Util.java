package ch.usi.dslab.bezerra.chirper.util;

import java.io.FileReader;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import ch.usi.dslab.bezerra.chirper.util.RicardoNetworkGenerator.RandomUser;

public class Util {

   public static long get8bytesSHA1(byte[] input) {
      MessageDigest md = null;
      try {
         md = MessageDigest.getInstance("SHA-1");
      } catch (NoSuchAlgorithmException e) {
         e.printStackTrace();
      }
      byte[] sha1 = md.digest(input);
      long retval = java.nio.ByteBuffer.wrap(sha1).getLong();
      return retval;
   }
   
   public static String get8bytesSHA1String(byte[] input) {
      return Long.toHexString(get8bytesSHA1(input));
   }
   
   /**
    * leastOdd(x): returns the number itself, if it's odd, or the closest lower odd number, otherwise.
    * e.g.: leastOdd(1) = 1, leastOdd(2) = 1, leastOdd(3) = 3, leastOdd(4) = 3 and so on
    * 
    * @param x
    * @return (x % 2 != 0) ? x : (x - 1)
    */
   
   public static int leastOdd(int x) {
      if (x % 2 != 0) return x;
      else            return x - 1;
   }
   
   public static List<RandomUser> loadSocialNetwork(String socialNetworkFile) {
      try {
         System.out.print("Loading social network from file " + socialNetworkFile + "...");
         
         List<RandomUser> allUsers = null;

         JSONParser parser = new JSONParser();

         JSONObject j_network = (JSONObject) parser.parse(new FileReader(socialNetworkFile));
         JSONArray  j_allUsers = (JSONArray) j_network.get("allUsers");
         allUsers = new ArrayList<RandomUser>(j_allUsers.size());
         
         for (Object obj_j_user : j_allUsers) {
            JSONObject j_user = (JSONObject) obj_j_user;
            RandomUser user = new RandomUser();
            user.userId      = ((Long) j_user.get("userId"     )).intValue();
            user.partitionId = ((Long) j_user.get("partitionId")).intValue();
            JSONArray j_followers = (JSONArray) j_user.get("followers");
            for (Object obj_j_follower : j_followers) {
               int followerId = ((Long) obj_j_follower).intValue();
               user.followers.add(followerId);
            }
            allUsers.add(user);
         }
         
         System.out.println(" done.");

         return allUsers;
         
      } catch (ParseException | IOException e) {
         e.printStackTrace();
         System.exit(1);
      }
      return null;
   }

}
