package ch.usi.dslab.bezerra.chirper.benchmarks;

public class RetwisUserCreator {

   static void createRetwisUsers(String socialNetworkFile, String redisHost, int redisPort, String idMappingFile) {
      RetwisBenchClient client = RetwisBenchClient.newClientTransactional(redisHost, redisPort);
//      client.createUsers(numUsers, fileName);
      client.loadSocialNetwork(socialNetworkFile, idMappingFile);
   }
   
   static String[] args;
   
   static void sa(String[] args) {
      RetwisUserCreator.args = args;
   }
   
   static int ipi(int i) {
      return Integer.parseInt(args[i]);
   }
   
   public static void main(String[] args) {
      sa(args);
      String socialNetworkFile = args[0];
      String redisHost = args[1];
      int redisPort = ipi(2);
      String idMappingFile = args[3];
      createRetwisUsers(socialNetworkFile, redisHost, redisPort, idMappingFile);
      System.exit(0);
   }
   
}
