/*

 Chirper - A twitter clone based on Eyrie 2 (Optimistic S-SMR)
 Copyright (C) 2014, University of Lugano
 
 This file is part of Chirper.
 
 Chirper is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.chirper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ch.usi.dslab.bezerra.eyrie.LocalMethodCall;
import ch.usi.dslab.bezerra.eyrie.PRObject;
import ch.usi.dslab.bezerra.eyrie.Partition;
import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.bezerra.netwrapper.codecs.Codec;
import ch.usi.dslab.bezerra.netwrapper.codecs.CodecUncompressedKryo;

public class User extends PRObject {
   private static final long serialVersionUID = 1L;

   public static Map<Integer, User> usersMap = new HashMap<Integer, User>();
   
   int id;
   UserList  followeds;
   UserList  followers;
   PostsList posts;
   PostsList materializedTimeline;
   
   public User() {
      // default constructor for Kryo
   }
   
   public User(int userId) {
      if (usersMap.containsKey(userId))
         return;
      this.id = userId;
      usersMap.put(this.id, this);
      followeds = new UserList();
      followers = new UserList();
      posts     = new PostsList();
      materializedTimeline = new PostsList();
   }
   
   @LocalMethodCall
   public static User getUser(int userId) {
      return usersMap.get(userId);
   }
   
   @LocalMethodCall
   public void post(Post post) {
      posts.addPost(post);
   }

   public PostsList getUserPosts() {
      return posts;
   }
   
   @LocalMethodCall
   public boolean follow(int followedId) {
      return followeds.add(followedId);
   }
   
   @LocalMethodCall
   public boolean unfollow(int followedId) {
      return followeds.remove(followedId);
   }
   
   @LocalMethodCall
   public boolean addFollower(int followerId) {
      return followers.add(followerId);
   }
   
   @LocalMethodCall
   public void removeFollower(int followerId) {
      followers.remove(followerId);
   }
   
   @LocalMethodCall
   public UserList getFollowedList() {
      return followeds;
   }
   
   public UserList getFollowerList() {
      return followers;
   }
   
   @LocalMethodCall
   public void addToMaterializedTimeline(Post post) {
      if (followeds.contains(post.getPosterId()))
         materializedTimeline.addPost(post);
   }
   
   @LocalMethodCall
   public void addToMaterializedTimeline(PostsList posts) {
      for (Post post : posts.postsList)
         if (followeds.contains(post.getPosterId()))
            materializedTimeline.addPost(post);
   }
   
   @LocalMethodCall
   public PostsList getMaterializedTimeline() {
      return materializedTimeline;
   }

   @Override
   public Message getSuperDiff(List<Partition> destinations) {
      return new Message(this.posts, this.materializedTimeline, this.followers, this.followeds);
   }

   @Override
   public void updateFromDiff(Message objectDiff) {
      this.posts = (PostsList) objectDiff.getNext();
      this.materializedTimeline = (PostsList) objectDiff.getNext();
      this.followers = (UserList) objectDiff.getNext();
      this.followeds = (UserList) objectDiff.getNext();
   }
   
   @Override
   public PRObject deepClone() {
      Codec codec = new CodecUncompressedKryo();
      return (PRObject) codec.deepDuplicate(this);
   }
}
