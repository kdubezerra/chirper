/*

 Chirper - A twitter clone based on Eyrie 2 (Optimistic S-SMR)
 Copyright (C) 2014, University of Lugano
 
 This file is part of Chirper.
 
 Chirper is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.chirper.benchmarks;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import ch.usi.dslab.bezerra.chirper.Post;
import ch.usi.dslab.bezerra.chirper.util.RicardoNetworkGenerator.RandomUser;
import ch.usi.dslab.bezerra.chirper.util.Util;
import ch.usi.dslab.bezerra.retwis.RetwisClientInterface;
import ch.usi.dslab.bezerra.retwis.RetwisClientSimple;
import ch.usi.dslab.bezerra.retwis.RetwisClientTransactional;
import ch.usi.dslab.bezerra.retwis.RetwisPost;
import ch.usi.dslab.bezerra.eyrie.cs.CallbackHandler;
import ch.usi.dslab.bezerra.netwrapper.Message;

public class RetwisBenchClient implements ClientInterface {
   
   public static class OutstandingCommand implements Runnable {
      public final static int CREATE_USER = 0;
      public final static int POST = 1;
      public final static int FOLLOW = 2;
      public final static int UNFOLLOW = 3;
      public final static int GET_TIMELINE = 4;
      public final static int LOOKUP = 5;
      public final static int FOLLOW_NOP = 6;
      public final static int UNFOLLOW_NOP = 7;
      int commandType;
      Object[] args;
      RetwisBenchClient benchClient;
      RetwisClientInterface retwisClient;
      CallbackHandler handler;
      Object context;

      /**
       * @param args
       *           - arguments, exactly as they would be given to the Retwis
       *           method (not the ClientInterface method)
       */
      public OutstandingCommand(CallbackHandler handler, Object context, RetwisBenchClient benchClient, int commandType, Object... args) {
         this.handler = handler;
         this.context = context;
         this.benchClient = benchClient;
         this.retwisClient = benchClient.retwisClient;
         this.commandType = commandType;
         this.args = args;
      }

      @Override
      public void run() {
         switch (commandType) {
            case CREATE_USER: {
               Integer userId = (Integer) args[0];
               Long retwisId = retwisClient.register(Integer.toString(userId));
               benchClient.benchIdToRetwisIdMap.put(userId, retwisId);
               handler.handleCallback(new Message("OK"), context);
               break;
            }
            case POST: {
               Long retwisUserId = benchClient.benchIdToRetwisIdMap.get((Integer) args[0]);
               String post = (String) args[1];
               retwisClient.post(retwisUserId, post);
               handler.handleCallback(new Message("OK"), context);
               break;
            }
            case FOLLOW: {
               Long retwisFollowerId = benchClient.benchIdToRetwisIdMap.get((Integer) args[0]);
               Long retwisFollowedId = benchClient.benchIdToRetwisIdMap.get((Integer) args[1]);
               retwisClient.follow(retwisFollowerId, retwisFollowedId);
               handler.handleCallback(new Message("OK"), context);
               break;
            }
            case UNFOLLOW: {
               Long retwisFollowerId = benchClient.benchIdToRetwisIdMap.get((Integer) args[0]);
               Long retwisFollowedId = benchClient.benchIdToRetwisIdMap.get((Integer) args[1]);
               retwisClient.unfollow(retwisFollowerId, retwisFollowedId);
               handler.handleCallback(new Message("OK"), context);
               break;
            }
            case FOLLOW_NOP: {
               Long retwisFollowerId = benchClient.benchIdToRetwisIdMap.get((Integer) args[0]);
               Long retwisFollowedId = benchClient.benchIdToRetwisIdMap.get((Integer) args[1]);
               retwisClient.follow_NOP(retwisFollowerId, retwisFollowedId);
               handler.handleCallback(new Message("OK"), context);
               break;
            }
            case UNFOLLOW_NOP: {
               Long retwisFollowerId = benchClient.benchIdToRetwisIdMap.get((Integer) args[0]);
               Long retwisFollowedId = benchClient.benchIdToRetwisIdMap.get((Integer) args[1]);
               retwisClient.unfollow_NOP(retwisFollowerId, retwisFollowedId);
               handler.handleCallback(new Message("OK"), context);
               break;
            }
            case GET_TIMELINE: {
               Long retwisUserId = benchClient.benchIdToRetwisIdMap.get((Integer) args[0]);
               NavigableSet<RetwisPost> timeline = retwisClient.getTimeline(retwisUserId);
               List<Post> postTimeline = new ArrayList<Post>();
               for (RetwisPost rpost : timeline) {
                  Post post = new Post(Integer.parseInt(rpost.user), rpost.timestamp, rpost.message.getBytes());
                  postTimeline.add(post);
               }
               handler.handleCallback(new Message(postTimeline), context);
               break;
            }
         }
      }
   }
   
   
   public static RetwisBenchClient newClientSimple(String redisHost, int redisPort) {
      RetwisClientInterface retwisClient = new RetwisClientSimple(redisHost, redisPort);
      return new RetwisBenchClient(retwisClient, 0);
   }
   
   public static RetwisBenchClient newClientSimple(String redisHost, int redisPort, int numPermits) {
      RetwisClientInterface retwisClient = new RetwisClientSimple(redisHost, redisPort);
      return new RetwisBenchClient(retwisClient, numPermits);
   }
   
   public static RetwisBenchClient newClientTransactional(String redisHost, int redisPort) {
      RetwisClientInterface retwisClient = new RetwisClientTransactional(redisHost, redisPort);
      return new RetwisBenchClient(retwisClient, 0);
   }
   
   public static RetwisBenchClient newClientTransactional(String redisHost, int redisPort, int numPermits) {
      RetwisClientInterface retwisClient = new RetwisClientTransactional(redisHost, redisPort);
      return new RetwisBenchClient(retwisClient, numPermits);
   }

   private RetwisClientInterface retwisClient;
   private ExecutorService requestExecutor;
   private Map<Integer, Long> benchIdToRetwisIdMap;

   private RetwisBenchClient(RetwisClientInterface retwisClient, int numPermits) {
      this.requestExecutor = createExecutor(numPermits);
      this.retwisClient = retwisClient;
      benchIdToRetwisIdMap = new ConcurrentHashMap<Integer, Long>();
   }
  
   private ExecutorService createExecutor(int numPermits) {
      if (numPermits > 0)
         return Executors.newFixedThreadPool(numPermits);
      else if (numPermits == 0) {
         return new ThreadPoolExecutor(
               25, // core pool size
               25, // max pool size
               60, // idle timeout
               TimeUnit.SECONDS,
               new LinkedBlockingQueue<Runnable>()); // tasks queued when no thread is idle
      }
      else
         return null;
   }
   
   public void createUsers(int numUsers, String fileName) {
      System.out.print("Creating " + numUsers + " users...");
      final Semaphore allCreatedSignal = new Semaphore(1 - numUsers);
      CallbackHandler manyCreatesHandler = new CallbackHandler() {
         public void handleCallback(Object reply, Object context) {
            allCreatedSignal.release();
         }
      };
      for (int userId = 0 ; userId < numUsers ; userId++) {
         OutstandingCommand oc = new OutstandingCommand(manyCreatesHandler, null, this, OutstandingCommand.CREATE_USER, userId);
         requestExecutor.execute(oc);
      }
      allCreatedSignal.acquireUninterruptibly();
      if (fileName != null) saveIdMapToFile(fileName);
      System.out.println(" Done.");
   }
   
   public void loadSocialNetwork(String socialNetworkFileName, String idMappingFilename) {
      List<RandomUser> socialNetwork = Util.loadSocialNetwork(socialNetworkFileName);
      System.out.print("Loading social network:\n   Loading users...");
      int numUsers = socialNetwork.size();
      final Semaphore allCreatedSignal = new Semaphore(0);
      CallbackHandler manyCreatesHandler = new CallbackHandler() {
         public void handleCallback(Object reply, Object context) {
            allCreatedSignal.release();
         }
      };
      for (RandomUser user : socialNetwork) {
         OutstandingCommand oc = new OutstandingCommand(manyCreatesHandler, null, this, OutstandingCommand.CREATE_USER, user.userId);
         requestExecutor.execute(oc);
      }
      allCreatedSignal.acquireUninterruptibly(numUsers);
      
      
      System.out.print(" done.\n   Loading followers...");
      for (RandomUser user : socialNetwork) {
         final Semaphore allFollowedSignal = new Semaphore(0);
         CallbackHandler followReplyHandler = new CallbackHandler() {
            public void handleCallback(Object reply, Object context) {
               allFollowedSignal.release();
            }
         };
         for (int followerId : user.followers) {
            follow(followerId, user.userId, null, followReplyHandler, null);
            follow_NOP(followerId, user.userId, null, followReplyHandler, null);
         }
         allFollowedSignal.acquireUninterruptibly(2 * user.followers.size());
      }

      System.out.print(" done.\n   Loading posts...");
      final Semaphore allPostsSignal = new Semaphore(0);
      CallbackHandler postReplyHandler = new CallbackHandler() {
         public void handleCallback(Object reply, Object context) {
            allPostsSignal.release();
         }
      };
      for (RandomUser user : socialNetwork) {
         byte[] post = ("" + user.userId).getBytes();
         post(user.userId, post, null, postReplyHandler, null);
      }
      allPostsSignal.acquireUninterruptibly(numUsers);
      
      if (idMappingFilename != null) {
         System.out.print(" done.\n   Saving id->retwisId mapping to file " + idMappingFilename + "...");
         saveIdMapToFile(idMappingFilename);
      }
      
      System.out.println(" done.\nDone loading social network from file " + socialNetworkFileName + ".");
   }
   
   public void saveIdMapToFile(String fileName) {
      try {
         Map<Integer,Long> map = benchIdToRetwisIdMap;
         ObjectOutputStream fileObjectStream = new ObjectOutputStream(new FileOutputStream(fileName));
         fileObjectStream.writeObject(map);
         fileObjectStream.close();
      }
      catch (IOException e) {
         e.printStackTrace();
         System.exit(1);
      }
   }
   
   @SuppressWarnings("unchecked")
   public void loadIdMapFromFile(String fileName) {
      try {
         ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName));
         Map<Integer,Long> mapping = (Map<Integer,Long>) ois.readObject();
         ois.close();
         benchIdToRetwisIdMap = mapping;
      }
      catch (IOException | ClassNotFoundException e) {
         e.printStackTrace();
         System.exit(1);
      }
   }

   @Override
   public void createUser(int userId, CallbackHandler nonUsedHandler, CallbackHandler handler, Object context) {
      OutstandingCommand oc = new OutstandingCommand(handler, context, this, OutstandingCommand.CREATE_USER, userId);
      requestExecutor.execute(oc);
   }

   @Override
   public void post(int userId, byte[] post, CallbackHandler nonUsedHandler, CallbackHandler handler, Object context) {
      OutstandingCommand oc = new OutstandingCommand(handler, context, this, OutstandingCommand.POST, userId, new String(post));
      requestExecutor.execute(oc);
   }

   @Override
   public void follow(int followerId, int followedId, CallbackHandler nonUsedHandler, CallbackHandler handler, Object context) {
      OutstandingCommand oc = new OutstandingCommand(handler, context, this, OutstandingCommand.FOLLOW, followerId, followedId);
      requestExecutor.execute(oc);
   }
   
   @Override
   public void follow_NOP(int followerId, int followedId, CallbackHandler nonUsedHandler, CallbackHandler handler, Object context) {
      OutstandingCommand oc = new OutstandingCommand(handler, context, this, OutstandingCommand.FOLLOW_NOP, followerId, followedId);
      requestExecutor.execute(oc);
   }

   @Override
   public void unfollow(int followerId, int followedId, CallbackHandler nonUsedHandler, CallbackHandler handler, Object context) {
      OutstandingCommand oc = new OutstandingCommand(handler, context, this, OutstandingCommand.UNFOLLOW, followerId, followedId);
      requestExecutor.execute(oc);
   }
   
   @Override
   public void unfollow_NOP(int followerId, int followedId, CallbackHandler nonUsedHandler, CallbackHandler handler, Object context) {
      OutstandingCommand oc = new OutstandingCommand(handler, context, this, OutstandingCommand.UNFOLLOW_NOP, followerId, followedId);
      requestExecutor.execute(oc);
   }

   @Override
   public void getTimeline(int userId, CallbackHandler nonUsedHandler, CallbackHandler handler, Object context) {
      OutstandingCommand oc = new OutstandingCommand(handler, context, this, OutstandingCommand.GET_TIMELINE, userId);
      requestExecutor.execute(oc);
   }

}
